
## Before compilation install MKL, GSL, TBB (if not installed with intel components)

######## compiler ####################

CPP = g++

CPP_FLAGS = -O3 -g --std=c++11 -fopenmp  -Wall -fmessage-length=0
CPP_FLAGS_DBG = -O0 -g -ggdb --std=c++11 -fopenmp -Wall -fmessage-length=0


######## headers #####################
INCLUDE := -I$(HOME)/Projects/spinus/libs/wigxjpf/inc
INCLUDE += -I$(HOME)/Projects/spinus/libs/Eigen
INCLUDE += -I$(HOME)/Projects/Arpack++/include

MKL_INCLUDE = -I${MKLROOT}/include
INCLUDE += $(MKL_INCLUDE)

#########  libraries #################
EXT_LIBS := $(HOME)/Projects/spinus/libs/wigxjpf/lib/libwigxjpf.a

# Arpack libraries
EXT_LIBS += $(HOME)/Projects/Arpack++/external/libarpack.a 
EXT_LIBS += -lgfortran

# other
EXT_LIBS += -lgsl -lgslcblas  -ltbb

# MKL
MKL_LIBS :=

UNAME_S := $(shell uname -s)

#-lmkl_intel_lp64
ifeq ($(UNAME_S),Linux)
MKL_LIBS += -Wl,--start-group -L${MKLROOT}/lib/intel64/ -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -Wl,--end-group -lgomp -lpthread -lm -ldl
endif
ifeq ($(UNAME_S),Darwin)
MKL_LIBS += -L${MKLROOT}/lib/ -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl
endif

EXT_LIBS += $(MKL_LIBS) 
