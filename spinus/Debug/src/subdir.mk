################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/DensityMatrixDouble.cpp \
../src/DensityMatrixSingle.cpp \
../src/Game.cpp \
../src/HamiltonianBuilder.cpp \
../src/JJState.cpp \
../src/JJStateNode.cpp \
../src/MathHelper.cpp \
../src/MatrixBuilder.cpp \
../src/SpinDynamic.cpp \
../src/Spliner.cpp \
../src/Tensor.cpp \
../src/VarParameter.cpp \
../src/VectorUnsafe.cpp \
../src/helper.cpp 

OBJS += \
./src/DensityMatrixDouble.o \
./src/DensityMatrixSingle.o \
./src/Game.o \
./src/HamiltonianBuilder.o \
./src/JJState.o \
./src/JJStateNode.o \
./src/MathHelper.o \
./src/MatrixBuilder.o \
./src/SpinDynamic.o \
./src/Spliner.o \
./src/Tensor.o \
./src/VarParameter.o \
./src/VectorUnsafe.o \
./src/helper.o 

CPP_DEPS += \
./src/DensityMatrixDouble.d \
./src/DensityMatrixSingle.d \
./src/Game.d \
./src/HamiltonianBuilder.d \
./src/JJState.d \
./src/JJStateNode.d \
./src/MathHelper.d \
./src/MatrixBuilder.d \
./src/MatrixBuilderSparse.d \
./src/SpinDynamic.d \
./src/Spliner.d \
./src/Tensor.d \
./src/VarParameter.d \
./src/VectorUnsafe.d \
./src/helper.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CPP) $(CPP_FLAGS_LOC) $(INCLUDE) -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


