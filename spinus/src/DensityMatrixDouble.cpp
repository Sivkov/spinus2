/*
 * DensityMatrixDouble.cpp
 *
 *  Created on: Jan 26, 2016
 *      Author: isivkov
 */


#include "DensityMatrixDouble.h"
#include<iostream>
using namespace std;

namespace sps
{

DensityMatrixDouble::DensityMatrixDouble(DensityMatrixSingle *dmm1, DensityMatrixSingle *dmm2):
	dmm1(dmm1),
	dmm2(dmm2),
	spinInd1(dmm1->GetSpinIndices()[0]),
	spinInd2(dmm2->GetSpinIndices()[0]),
	isConfigured(true)
{
	// left has only triangle view
	leftVecs.resize(dmm1->GetMatrices().size(),VectorRowZ(dmm1->GetEigvecDim()));

	// right has full view
	rightVecs.resize(dmm2->GetDimensions() * dmm2->GetDimensions(),VectorRowZ(dmm2->GetEigvecDim()));
}


void DensityMatrixDouble::CalcDensityMatrix(MKL_Complex16 *eigenVec, int vecLength, MKL_Complex16 *outMatr)
{
	Eigen::setNbThreads(1);

	if(!isConfigured)
	{
		throw SpinException("Density matrix is not configured");
	}

	Eigen::Map<VectorZ> vec(eigenVec,vecLength);

	const cvector_triplet_IIpSM &mats1= dmm1->GetMatrices();
	const cvector_triplet_IIpSM &mats2= dmm2->GetMatrices();

	int dim1 = dmm1->GetDimensions();
	int dim2 = dmm2->GetDimensions();

#pragma omp parallel for
	for( int i = 0; i < (int)mats1.size(); i++ )
	{
		leftVecs[i]= vec.adjoint() * mats1[i].val3;
	}

	#pragma omp parallel for
	for( int i = 0; i < (int)mats2.size(); i++ )
	{
		int indI = (mats2[i].val1+dim2-1)/2;
		int indJ = (mats2[i].val2+dim2-1)/2;

		rightVecs[indI*dim2+indJ]= mats2[i].val3 * vec;

		if( indI != indJ )
		{
			rightVecs[indJ*dim2+indI]= mats2[i].val3.transpose() * vec;
		}
	}

#pragma omp parallel for
	for( int i = 0; i < (int)mats1.size(); i++ )
	{
		int indI1 = (mats1[i].val1+dim1-1)/2;
		int indJ1 = (mats1[i].val2+dim1-1)/2;

		for( int j = 0; j < (int)mats2.size(); j++ )
		{
			int indI2 = (mats2[j].val1+dim2-1)/2;
			int indJ2 = (mats2[j].val2+dim2-1)/2;

			int indI = indI1 * dim2 + indI2;
			int indJ = indJ1 * dim2 + indJ2;

			MKL_Complex16 val = leftVecs[i] * rightVecs[indI2 * dim2 + indJ2];

			outMatr[ indI * dim1 * dim2 + indJ ] = val;
			outMatr[ indJ * dim1 * dim2 + indI ] = std::conj(val);

			if(indI1 != indJ1)
			{
				int indI = indI1 * dim2 + indJ2;
				int indJ = indJ1 * dim2 + indI2;

				MKL_Complex16 val = leftVecs[i] * rightVecs[indJ2 * dim2 + indI2];

				outMatr[ indI * dim1 * dim2 + indJ ] = val;
				outMatr[ indJ * dim1 * dim2 + indI ] = std::conj(val);
			}
		}
	}
}

}
