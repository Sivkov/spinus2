/*
 * DensityMatrixDouble.h
 *
 *  Created on: Jan 26, 2016
 *      Author: isivkov
 */

#ifndef DENSITYMATRIXDOUBLE_H_
#define DENSITYMATRIXDOUBLE_H_

#include "DensityMatrixSingle.h"
#include<vector>

using namespace std;

namespace sps
{

class DensityMatrixDouble : public DMAbstract
{

	DensityMatrixDouble():dmm1(nullptr),dmm2(nullptr), spinInd1(-1), spinInd2(-1), isConfigured(false){}

public:
	DensityMatrixDouble(DensityMatrixSingle *dmm1, DensityMatrixSingle *dmm2);

	void CalcDensityMatrix(MKL_Complex16 *eigenVec, int vecLength, MKL_Complex16 *outMatr);

	int GetDimensions(){ return dmm1->GetDimensions() * dmm2->GetDimensions(); }

	vector<int> GetSpinIndices(){ return {spinInd1,spinInd2}; }

private:
	DensityMatrixSingle *dmm1;
	DensityMatrixSingle *dmm2;

	int spinInd1;
	int spinInd2;

	bool isConfigured;

	vector<VectorRowZ> leftVecs;
	vector<VectorZ> rightVecs;
};
}



#endif /* DENSITYMATRIXDOUBLE_H_ */
