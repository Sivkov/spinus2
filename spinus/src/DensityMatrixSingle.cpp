/*
 * DensityMatrixSingle.cpp
 *
 *  Created on: Jan 23, 2016
 *      Author: isivkov
 */


#include "DensityMatrixSingle.h"
#include <iostream>
#include "Timer.h"

namespace sps
{
DensityMatrixSingle::DensityMatrixSingle(JJState *jjs, int spinInd)
:jjs(jjs),
 spinInd(spinInd),
 isConfigured(false)
{
	twoJ=jjs->getInitTwoJ()[spinInd];
	dim=twoJ+1;
	initEmptyDMMatrices();
}


void DensityMatrixSingle::initEmptyDMMatrices()
{
	for(int i = 0; i <= twoJ; i++)
	{
		for(int j = 0; j <= i; j++)
		{
			int twoMi = 2*i-twoJ;
			int twoMj = 2*j-twoJ;

			dmMatrices.push_back({twoMi,twoMj,SparseMatrixZ(jjs->GetBasisDimension(),jjs->GetBasisDimension())});
		}
	}
}

DensityMatrixSingle::~DensityMatrixSingle()
{

}


void DensityMatrixSingle::CalcDensityMatrix(MKL_Complex16 *eigenVec, int vecLength, MKL_Complex16 *outMatr)
{
	Eigen::setNbThreads(1);

	if(!isConfigured)
	{
		throw SpinException("Density matrix is not configured");
	}

	Eigen::Map<VectorZ> vec(eigenVec,vecLength);

	#pragma omp parallel for
	for( int i = 0; i < (int)dmMatrices.size(); i++ )
	{
		SparseMatrixZ &mat = dmMatrices[i].val3;

		MKL_Complex16 val = (mat*vec).dot(vec);

		int indI = (dmMatrices[i].val1+twoJ)/2;
		int indJ = (dmMatrices[i].val2+twoJ)/2;

		outMatr[ indI * dim + indJ ] = val;

		if( indI != indJ )
		{
			outMatr[ indJ * dim + indI ] = std::conj(val); // may be not needed
		}
	}
}

int DensityMatrixSingle::GetDimensions()
{
	return dim;
}

vector<int> DensityMatrixSingle::GetSpinIndices()
{
	return {spinInd};
}

void DensityMatrixSingle::BuildMatrix()
{
	if( jjs == nullptr )
	{
		throw SpinException("JJState is not set to DensityMatrix class");
	}

	if(isConfigured) return;

	MatrixBuilder mb(jjs);

	for(int k = 0; k <= twoJ; k++)
	{
		for(int q = -k; q <= k; q++)
		{
			SingleTensor tens(spinInd,jjs->GetNumParticles(),2*k,2*q,std::sqrt( (2*k+1) / sqrme (twoJ, 2*k) ) );

			SparseMatrixZ mat(jjs->GetBasisDimension(),jjs->GetBasisDimension());

			Timer t("DensityMatrixSingle::BuildMatrix BuildFromTensorSparse ");
			mb.BuildFromTensorSparse(tens,mat);
			t.stop();

			if(mat.nonZeros()==0) continue;

			Timer t2("DensityMatrixSingle::BuildMatrix add matrix ");
			for(int i = 0; i < (int)dmMatrices.size(); i++)
			{
				int twoMi = dmMatrices[i].val1;
				int twoMj = dmMatrices[i].val2;

				int coef = (((twoJ - 2*k + twoMj )>>1)&1) ? -1.0 : 1.0;

				double s3j = std::sqrt(2 * k + 1) * coef * gsl_sf_coupling_3j(twoJ, 2*k, twoJ, twoMi, 2*q, -twoMj);

				if(s3j == 0.0) continue;

				dmMatrices[i].val3 += mat * s3j;
			}
			t2.stop();
		}
	}

	for(auto& m: dmMatrices)
	{
		m.val3.makeCompressed();
	}
	isConfigured=true;

}
}

