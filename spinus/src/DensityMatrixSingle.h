/*
 * DensityMatrixSingle.h
 *
 *  Created on: Jan 23, 2016
 *      Author: isivkov
 */

#ifndef DENSITYMATRIXSINGLE_H_
#define DENSITYMATRIXSINGLE_H_

#include <gsl/gsl_math.h>
#include <gsl/gsl_sf.h>

#include "SpinSinTypes.h"

//#include "Eigen/SparseCore"
#include "DMAbstract.h"
#include "JJState.h"
#include "SingleTensor.h"
#include "MathHelper.h"
#include "MatrixBuilder.h"


namespace sps
{


typedef Eigen::Triplet<SparseMatrixZ*> TripletSparseMatZ;
//typedef tbb::concurrent_vector<TripletSparseMatZ> VectorTripletSpMatZ;



//typedef tbb::concurrent_vector<MKL_Complex16> CVectorZ;

class DensityMatrixSingle : public DMAbstract
{

	DensityMatrixSingle():jjs(nullptr),dim(0),spinInd(-1),isConfigured(false),twoJ(-1){};

public:
	DensityMatrixSingle(JJState *jjs, int spinInd);

	~DensityMatrixSingle();

	void CalcDensityMatrix(MKL_Complex16 *eigenVec, int vecLength, MKL_Complex16 *outMatr);

	int GetDimensions();

	vector<int> GetSpinIndices();

	void BuildMatrix();

	const cvector_triplet_IIpSM& GetMatrices() const { return dmMatrices; }

	long GetEigvecDim(){ return jjs->GetBasisDimension(); }

private:

	void initEmptyDMMatrices();

	JJState *jjs;

	int dim;

	int spinInd;

	bool isConfigured;

	cvector_triplet_IIpSM dmMatrices;


	int twoJ;
};

}


#endif /* DENSITYMATRIXSINGLE_H_ */
