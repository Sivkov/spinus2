/*
 * Game.cpp
 *
 *  Created on: Jul 15, 2015
 *      Author: isivkov
 */

#include "Game.h"
#include "Timer.h"

namespace sps
{

Game::Game()
{
	cfg=nullptr;
}

Game::~Game()
{
	if(cfg!=nullptr)
		delete cfg;
}


void Game::readConfig(string inputFile)
{
	// --------- default config ------------------
	cfg=new GlobalConfig(
	{

		{},
		0,
		{},
		{},
		false,
		false,
		"",
		{},
		{
				0,
				0,
				0,
				0,
				{},
				"",
				0,
				false,
				"",
				false,
				""
		},
		{
				true,
				false,
				true,
				true,
				1,
				0,
				{},
				{},
				"",
				"",
				"",
				false
		},
		{}

	});


	// --------- read config from file ------------------
	cout<<"read input file ... ";

	InputParser prs;

	prs.LoadConfigFromFile(cfg,inputFile);

	cout<<"done"<<endl;
}



void Game::Run(string inputFile)
{
	readConfig(inputFile);

	// ------------ init spins ----------------------
	cout<<"init basis ... ";

	JJState *jjs = new JJState(cfg->spins[0]);
	for(int i = 1; i < (int)cfg->spins.size();i++)
	{
		jjs->PlusJ(cfg->spins[i]);
	}

	jjs->BuildBasis();

	cout<<"done"<<endl;


	// ------------- build hamiltonian --------------
	cout<<"build operators ... "<<endl;

	HamiltonianBuilder *hb = new HamiltonianBuilder(jjs);

	hb->SetConfig(cfg);

	hb->Configure();

	cout<<"done"<<endl;


	// ------------- calculations --------------
	if(cfg->isStaticCalc)
	{
		cout<<"START STATIC CALCULATION ... "<<endl;

//		SpinStatic *calc = new SpinStatic();
//
//		calc->SetConfig(cfg->statConfig);
//		calc->SetJJState(jjs);
//		calc->SetHamBuilder(hb);
//
//		calc->Calc();
//
//		cout<<"DONE"<<endl;
//
//		delete calc;
	}

	if(cfg->isDynamicCalc)
	{
		cout<<"START DYNAMIC CALCULATION ... "<<endl;

		SpinDynamic *calc=new SpinDynamic();

		calc->SetConfig(cfg->dynConfig);
		calc->SetJJState(jjs);
		calc->SetHamBuilder(hb);

		calc->Calc();

		cout<<"DONE"<<endl;

		delete calc;
	}

	delete hb;
	delete jjs;

	Timer::print();
}

}
