/*
 * GlobalConfig.h
 *
 *  Created on: Sep 25, 2015
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_GLOBALCONFIG_H_
#define INPUTSYSTEM_GLOBALCONFIG_H_

#include <vector>
#include <string>

#include "QOperator.h"
#include "VarParameter.h"
#include "SpinStaticConfig.h"
#include "SpinDynamicConfig.h"

using namespace std;

namespace sps
{

// yes, I know, it is not good
class GlobalConfig
{
public:
	vector<int> spins;
	int numberOfSpins;

	vector<QOperator*> operators;
	vector<VarParameter*> parameters;

	bool isDynamicCalc; // static == true
	bool isStaticCalc;

	string parDataFile;
	vector<string> parNames;
	
	SpinDynamicConfig dynConfig;
	SpinStaticConfig statConfig;

	vector<VarParameter*> observables;

	vector<int> singleDMInds;
	vector<pair<int,int>> doubleDMInds;
	vector<int> spinNumsForMagMom;
};


}


#endif /* INPUTSYSTEM_GLOBALCONFIG_H_ */
