/*
 * OperatorFabrique.cpp
 *
 *  Created on: Jun 3, 2015
 *      Author: isivkov
 */


#include <algorithm>

#include "HamiltonianBuilder.h"
#include "MatrixBuilder.h"
#include "SpinQOp.h"
//#include "FiniteDiffSolver.h"

namespace sps
{
HamiltonianBuilder::~HamiltonianBuilder()
{
	for(auto dms: singleDMs)
	{
		delete dms;
	}

	for(auto dmd: doubleDMs)
	{
		delete dmd;
	}
}

//--------------------------------------------------------------------------------
bool TensorToParamsCompare(const pair<Tensor*, VarParameter*> &ttp1, const pair<Tensor*, VarParameter*> &ttp2)
{
	return ( (*ttp1.first) < (*ttp2.first));
}


//--------------------------------------------------------------------------------
void HamiltonianBuilder::AddTensor(Tensor *tensor)
{
	tensorStore.push_back(tensor);
}


//--------------------------------------------------------------------------------
void HamiltonianBuilder::AddTensor(std::vector<Tensor*> tensors)
{
	tensorStore.insert(tensorStore.end(),tensors.begin(), tensors.end());
}

//--------------------------------------------------------------------------------
void HamiltonianBuilder::AddVarParameter(vector<VarParameter*> varParams)
{
	varParamStore.insert(varParamStore.end(),varParams.begin(),varParams.end());
}

//--------------------------------------------------------------------------------
void HamiltonianBuilder::AddVarParameter(VarParameter *varParam)
{
	varParamStore.push_back(varParam);
}


//--------------------------------------------------------------------------------
void HamiltonianBuilder::AddQOperator(QOperator *qOp)
{
	qOpStore.push_back(qOp);
}


//--------------------------------------------------------------------------------
vector<VarParameter*>& HamiltonianBuilder::GetVarParams()
{
	return varParamStore;
}




//--------------------------------------------------------------------------------
bool HamiltonianBuilder::GetConfigured()
{
	return configured;
}

//--------------------------------------------------------------------------------
void HamiltonianBuilder::Configure()
{
	long int size;

	cout<<"buid Hamiltonian..."<<endl;

	ConfigureHam();

	size = 0;
	for(auto param: globalCfg->parameters)
	{
		auto& m = param->GetMatrixUnsafe();
		size += m.nonZeros() * sizeof(SparseMatrixZ::Scalar) +
				m.outerSize() * sizeof(SparseMatrixZ::Index) +
				m.innerSize() * sizeof(SparseMatrixZ::Index);
	}

	cout<<"number of matrices: "<< globalCfg->parameters.size()<<"  size: "<<"size: "<<size / 1024.<<" KB"<<endl;

	cout<<"buid matrices for observables..."<<endl;

	ConfigureObserv();

	size = 0;
	for(auto m: observMatrs)
	{
		size += m.nonZeros() * sizeof(SparseMatrixZ::Scalar) +
				m.outerSize() * sizeof(SparseMatrixZ::Index) +
				m.innerSize() * sizeof(SparseMatrixZ::Index);
	}

	cout<<"number of matrices: "<< observMatrs.size()<<"  size: "<<size / 1024.<<" KB"<<endl;

	cout<<"buid density matrices..."<<endl;

	ConfigureDM();

	size = 0;
	int num = 0;
	for(auto* dm: singleDMs)
	{
		for(auto& op: dm->GetMatrices())
		{
			num++;
			auto& m = op.val3;
			size += m.nonZeros() * sizeof(SparseMatrixZ::Scalar) +
					m.outerSize() * sizeof(SparseMatrixZ::Index) +
					m.innerSize() * sizeof(SparseMatrixZ::Index);
		}
	}

	cout<<"number of matrices: "<< num<<"  size: "<<size / 1024.<<" KB"<<endl;

	cout<<"buid spin matrices..."<<endl;

	ConfigureSpinMatrices();

	size = 0;
	for(auto m: spinMatrs)
	{
		size += m.nonZeros() * sizeof(SparseMatrixZ::Scalar) +
				m.outerSize() * sizeof(SparseMatrixZ::Index) +
				m.innerSize() * sizeof(SparseMatrixZ::Index);
	}

	cout<<"number of matrices: "<< spinMatrs.size()<<"  size: "<<size / 1024.<<" KB"<<endl;
}

void HamiltonianBuilder::ConfigureHam()
{
	// setup MatrixBuilder to calculate matrices from tensors
	MatrixBuilder mb;
	mb.SetJJState(jjState);

	for(auto param: globalCfg->parameters)
	{
		mb.BuildFromTensorSparse(param->GetTensors(), param->GetMatrixUnsafe(), true);
		param->GetMatrixUnsafe().makeCompressed();
	}

	varParamStore = globalCfg->parameters;

	this->configured=true;
}

//--------------------------------------------------------------------------------
void HamiltonianBuilder::ConfigureSpinMatrices()
{
	//TODO move this to Hamiltonian Builder
	// create spin matrices
	MatrixBuilder mb;
	mb.SetJJState(jjState);

	for(int i=0;i<globalCfg->spinNumsForMagMom.size(); i++)
	{
		SpinQOp sop(globalCfg->spinNumsForMagMom[i], jjState->GetNumParticles());

		string comps[]={"_x","_y","_z"};

		for(int j=0; j<3; j++)
		{
			std::stringstream s;
			s<<"S"<<globalCfg->spinNumsForMagMom[i]<<comps[j];

			spinNames.push_back(s.str());

			SparseMatrixZ spinmat;

			mb.BuildFromTensorSparse(sop.GetComponents(j),spinmat);

			spinmat.makeCompressed();

			spinMatrs.push_back(std::move(spinmat));
		}
	}
}

//--------------------------------------------------------------------------------
void HamiltonianBuilder::ConfigureObserv()
{
	MatrixBuilder matBldr;
	matBldr.SetJJState(jjState);

	for(int i=0;i< globalCfg->observables.size() ; i++)
	{
		observNames.push_back(globalCfg->observables[i]->GetName());

		vector<Tensor*> tensors = globalCfg->observables[i]->GetTensors();

		SparseMatrixZ mat;

		matBldr.BuildFromTensorSparse(tensors,mat);

		mat.makeCompressed();

		observMatrs.push_back(std::move(mat));
	}
}

//--------------------------------------------------------------------------------
DensityMatrixSingle *HamiltonianBuilder::getSingleDM(int spinInd)
{
	auto it = std::find_if(singleDMs.begin(),singleDMs.end(),
			[spinInd](DensityMatrixSingle *dm)->bool
			{
				if (dm->GetSpinIndices()[0]==spinInd)
				{
					return true;
				}

				return false;
			});

	if ( it==singleDMs.end() )
	{
		DensityMatrixSingle *dm= new DensityMatrixSingle(jjState,spinInd);
		dm->BuildMatrix();
		return dm;
	}
	else
	{
		return *it;
	}
}


//--------------------------------------------------------------------------------
void HamiltonianBuilder::ConfigureDM()
{
	for(auto i: globalCfg->singleDMInds)
	{
		DensityMatrixSingle* dm = getSingleDM(i);

		singleDMs.push_back(dm);
	}

	for(auto p: globalCfg->doubleDMInds)
	{
		DensityMatrixSingle* dm1 = getSingleDM(p.first);
		DensityMatrixSingle* dm2 = getSingleDM(p.second);

		DensityMatrixDouble *dmd = new DensityMatrixDouble(dm1,dm2);

		doubleDMs.push_back(dmd);
	}
}
}
