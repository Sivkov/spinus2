/*
 * HeisenbergQOp.h
 *
 *  Created on: Jun 11, 2015
 *      Author: isivkov
 */

#ifndef HEISENBERGQOP_H_
#define HEISENBERGQOP_H_

#include <cmath>

#include "QOperator.h"
#include "PairTensor.h"


namespace sps
{

class HeisenbergQOp: public QOperator
{
public:
	HeisenbergQOp(int numS1=0,
			int numS2=1,
			int numTot=2,
			double J=1, std::string name="")
	:QOperator(name), J(J)
	{
		tensors = std::vector<Tensor*>(1,nullptr);

		if(J!=0)
			tensors[0] = new PairTensor(numS1, numS2, numTot, 2, 2, 0, 0, J* 2.0 * std::sqrt(3.0)) ;

		// add new tensors in store
		StoreLocator<Tensor>::GetStore()->Add(tensors);
	}

	virtual std::vector<Tensor*> GetComponents(int num)
		{
			if(num==0)
			{
				return tensors;
			}
			else
			{
				return std::vector<Tensor*>();
			}
		}

private:
	double J;
};

}

#endif /* HEISENBERGQOP_H_ */
