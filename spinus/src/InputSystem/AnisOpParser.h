/*
 * AnisOpParser.h
 *
 *  Created on: Oct 1, 2015
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_ANISOPPARSER_H_
#define INPUTSYSTEM_ANISOPPARSER_H_


#include "OperatorParser.h"


namespace sps
{

class AnisOpParser: public sps::OperatorParser
{
public:
	AnisOpParser(string keyWord):
		OperatorParser(keyWord),
		rangeSpins("range"),
		minRecLength(5)
		{}

	virtual ~AnisOpParser(){}


	virtual RecordInfo readLineRecord(GlobalConfig *config, RecordInfo inpinfo);
	virtual RecordInfo readBlockRecord(GlobalConfig *config, RecordInfo inpinfo){ return RecordInfo{RecordType::ReadError}; };

	void createRangeSpinsOP(GlobalConfig *config, int ind, int ind2, double D, double E, bool isParD, bool isParE, string parD, string parE);
	void createSingleOp(GlobalConfig *config, int ind1, double D, double E, bool isParD, bool isParE, string parD, string parE);

private:
	string rangeSpins;
	int minRecLength;
};

}


#endif /* INPUTSYSTEM_ANISOPPARSER_H_ */
