/*
 * DensityMatrixParser.cpp
 *
 *  Created on: Jan 26, 2016
 *      Author: isivkov
 */




#include "DensityMatrixParser.h"

namespace sps
{

RecordInfo DensityMatrixParser::Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	int n = config->numberOfSpins;

	if(config->numberOfSpins == 0 || config->spins.size()==0)
	{
		throw SpinException("DensityMatrixParser: error, spin array is not set ");
	}

	RecordInfo rinfo = findRecordByKeyWord(keyWord, scriptTokens, start, stop);

	if( rinfo.type == RecordType::NotFound )
	{
		return rinfo;
	}

	vector<int> singleInds;
	vector<int> doubleInds;


	// Parse single DM indices
	vector<string>::iterator nextStart = start;

	RecordInfo dmri = this->readArray(singleInds,singleDM,scriptTokens,nextStart,stop);

	//-------------------------------------------------------------------
	while(dmri.type != RecordType::NotFound)
	{
		//add one ind
		if(singleInds.size()==1 &&
				std::find(config->singleDMInds.begin(),config->singleDMInds.end(),singleInds[0]) == config->singleDMInds.end())
		{
			checkI(singleInds[0],n);

			config->singleDMInds.push_back(singleInds[0]);
		}

		// add set of inds
		if(singleInds.size() > 1)
		{
			checkI(singleInds[0],n);
			checkI(singleInds[1],n);

			for(int i=singleInds[0];i<=singleInds[1];i++)
			{
				checkI(i,n);

				if(std::find(config->singleDMInds.begin(),config->singleDMInds.end(),i) == config->singleDMInds.end())
				{
					config->singleDMInds.push_back(i);
				}
			}
		}

		// read next
		nextStart = dmri.out_end;

		singleInds.clear();

		dmri = this->readArray(singleInds,singleDM,scriptTokens,nextStart,stop);
	}



	// Parse double DM indices
	nextStart = start;

	dmri = this->readArray(doubleInds,doubleDM,scriptTokens,nextStart,stop);

	//--------------------------------------------------------------
	while(dmri.type != RecordType::NotFound)
	{
		// add one pair of inds
		if(doubleInds.size()==2 &&
				std::find(config->doubleDMInds.begin(),
						config->doubleDMInds.end(),
						pair<int,int>(doubleInds[0],doubleInds[1])) == config->doubleDMInds.end())
		{
			checkI(doubleInds[0],doubleInds[1],n);

			config->doubleDMInds.push_back(pair<int,int>(doubleInds[0],doubleInds[1]));
		}

		// add set of pairs of inds
		if(doubleInds.size() > 2)
		{
			int step = doubleInds[2];

			checkI(step,n);
			checkI(doubleInds[0],doubleInds[1],n);

			for(int i=doubleInds[0]; i<=doubleInds[1]-step; i+=step)
			{
				checkI(i,i+step,n);

				if(std::find(config->doubleDMInds.begin(),config->doubleDMInds.end(),pair<int,int>(i,i+step)) == config->doubleDMInds.end())
				{
					config->doubleDMInds.push_back(pair<int,int>(i,i+step));
				}
			}
		}

		nextStart = dmri.out_end;

		doubleInds.clear();
		dmri = this->readArray(doubleInds,doubleDM,scriptTokens,nextStart,stop);
	}
	return rinfo;
}

}
