/*
 * GaussParDataLoader.h
 *
 *  Created on: Jan 15, 2016
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_GAUSSPARDATALOADER_H_
#define INPUTSYSTEM_GAUSSPARDATALOADER_H_


#include "ParDataLoader.h"

namespace sps
{


class GaussParDataLoader: public ParDataLoader
{
public:
	GaussParDataLoader(string keyWord) : ParDataLoader(keyWord){}

	RecordInfo LoadParData(VarParameter *par, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
	{
		Parser* parser = GetParser();

		vector<double> params; // params = t0, tw, p0, step

		RecordInfo rinfo = parser->readArray(params,parser->GetKeyWord(), scriptTokens, start, stop);

		if(params.size() < 4)
			throw SpinException("Gauss data record has wrong number of parameters");

		int n = ( params[0] + params[1]*6.) / params[3] + 1; // (t0 + tw * 6) / step + 1 -- empirical

		vector<double> pdat;

		pdat.clear();

		for(int i = 0; i < n; i++)
		{
			pdat.push_back(gauss(i*params[3], params));
		}

		par->SetParData(std::move(pdat));
		par->SetArgStep(params[3]);

		return rinfo;
	}

private:


	double gauss(double t, vector<double> &pv) //pv: t0, tw, p0
	{
		double sigma = (t - pv[0])/pv[1];
		return pv[2] * std::exp(-0.5 * sigma*sigma );
	}


};

}



#endif /* INPUTSYSTEM_GAUSSPARDATALOADER_H_ */
