/*
 * HamiltonianParser.cpp
 *
 *  Created on: Oct 3, 2015
 *      Author: isivkov
 */

#include "HamiltonianParser.h"

namespace sps
{



HamiltonianParser::~HamiltonianParser()
{
//	for(auto p: parsers)
//	{
//		delete p;
//	}
}


void HamiltonianParser::SetDefaultParName(string name)
	{
		defaultParName = name;

		for(auto opp: parsers)
		{
			opp->SetDefaultParName(defaultParName);
		}
	}


RecordInfo HamiltonianParser::Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	if(config->numberOfSpins == 0 || config->spins.size()==0)
	{
		throw SpinException("HamiltonianParser: error, spin array is not set ");
	}

	RecordInfo rinfo = findRecordByKeyWord(keyWord, scriptTokens, start, stop);

	if( rinfo.type == RecordType::NotFound )
	{
		return rinfo;
	}

	if( rinfo.type != RecordType::Block )
	{
		throw SpinException("Hamiltonian record is not a block");
	}

	for(auto parser: parsers)
	{
		RecordInfo oprinfo; 
		vector<string>::iterator start = rinfo.out_start;
		while( (oprinfo = parser->Parse(config, scriptTokens, start, rinfo.out_end)).type != RecordType::NotFound )
		{
			start = oprinfo.out_end;
		}		
	}

	return rinfo;
}


void HamiltonianParser::AddOpParser(OperatorParser* oparser)
{
	parsers.push_back(oparser);
}
}

