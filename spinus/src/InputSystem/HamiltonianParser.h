/*
 * HamiltonianParser.h
 *
 *  Created on: Oct 3, 2015
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_HAMILTONIANPARSER_H_
#define INPUTSYSTEM_HAMILTONIANPARSER_H_

#include "OperatorParser.h"
#include "AnisOpParser.h"
#include "HeisOpParser.h"
#include "ZeemanOpParser.h"

namespace sps
{

class HamiltonianParser : public Parser
{
public:
	HamiltonianParser(string keyWord, string defaultParName="main"):
		Parser(keyWord),
		defaultParName(defaultParName)	{}

	string GetDefaultParName(){ return defaultParName; }
	void SetDefaultParName(string name);

	virtual ~HamiltonianParser();

	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop);
	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start){ return Parser::Parse(config, scriptTokens,start);}
	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens){ return Parser::Parse(config, scriptTokens);}

	void AddOpParser(OperatorParser* oparser);
	//virtual RecordInfo readLineRecord(GlobalConfig *config, RecordInfo inpinfo);
	//virtual RecordInfo readBlockRecord(GlobalConfig *config, RecordInfo inpinfo);


protected:
	string defaultParName;
	vector<OperatorParser*> parsers;
};

}


#endif /* INPUTSYSTEM_HAMILTONIANPARSER_H_ */
