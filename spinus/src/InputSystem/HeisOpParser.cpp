/*
 * HeisOpParser.cpp
 *
 *  Created on: Sep 30, 2015
 *      Author: isivkov
 */

#include "HeisOpParser.h"

namespace sps
{


//----------------------------------------------------------------------------------------------------
RecordInfo HeisOpParser::readLineRecord(GlobalConfig *config, RecordInfo inpinfo)
{
	vector<string>::iterator it = inpinfo.out_start;

	if(inpinfo.type == RecordType::NotFound || inpinfo.type == RecordType::ReadError)
		return inpinfo;

	// read operator record to measure size (may be it is stupid)
	vector<string> subv(inpinfo.out_start,inpinfo.out_end);

	if ((int)subv.size() < minRecLength) // because minimum format is  'heis 0 1 J;'
	{
		throw SpinException("HeisOpParser: error, Heisenberg operator is wrong: ... '" + *it + "' ...");
	}

	// read spin indices
	int ind1, ind2;

	try
	{
		++it;
		ind1 = stoi(*it);

		++it;
		ind2 = stoi(*it);
	}
	catch(exception &ex) // bugaga
	{
		throw SpinException("HeisOpParser: error, Heisenberg operator is wrong: ... '" + *it + "' ...");
	}

	// try to read 3d parameter
	// if it is number - fine, if not - we need to make parameterized operator
	double J=1.0;
	string param;
	bool isParam=false;

	try
	{
		++it;
		J = stod(*it);
	}
	catch(invalid_argument &ex) // ok, it is parameter
	{
		if( (*it).find_first_of(delimiters) != string::npos ) // special case if user is stupid :-)
		{
			throw SpinException("HeisOpParser: error, Heisenberg operator is wrong: ... '" + *(it-2) + " " + *(it-1) + *it + "' ...");
		}

		isParam=true;
		param = *it;
		J=1.0;
	}

	// check if 4th parameter is exist
	++it;

	if((*it)==lineCloseChar || (*it)==blockCloseChar) // doesn't exist, then create only one operator
	{
		createSingleOp(config, ind1, ind2, J, isParam, param);
		return inpinfo;
	}

	if((*it)== chainConn) // chain connected spins
	{
		createChainConnectedOp(config, ind1, ind2, J, isParam, param);
		return inpinfo;
	}

	if((*it)== fullConn) // full connected spins
	{
		createFullConnectedOP(config, ind1, ind2, J, isParam, param);
		return inpinfo;
	}



	// if we are here, then something seriously wrong
	throw SpinException("HeisOpParser: error, Heisenberg operator is wrong: ... '" + *(it-2) + " " + *(it-1) + *it + "' ...");

	return inpinfo;
}


//----------------------------------------------------------------------------------------------------
void HeisOpParser::createChainConnectedOp(GlobalConfig *config, int ind1, int ind2, double J, bool isParam=false, string parName="")
{
	VarParameter::State parState = VarParameter::Dynamic;

	if(!isParam)
	{
		parName = defaultParName;
		parState = VarParameter::Fixed;
	}

	VarParameter* par = getVarParam(config, parName, parState);

	int inc = ind1<ind2 ? 1 : -1;

	for(int i = ind1; i != ind2; i+=inc)
	{
		HeisenbergQOp *op = new HeisenbergQOp(i, i+inc, config->numberOfSpins, J, "heis");
		StoreLocator<QOperator>::GetStore()->Add(op); // add new operators to store

		vector<Tensor*> comp = op->GetComponents(0);

		par->AddTensor(comp);

		config->operators.push_back(op);
	}
}


//----------------------------------------------------------------------------------------------------
void HeisOpParser::createFullConnectedOP(GlobalConfig *config, int ind1, int ind2, double J,bool isParam=false, string parName="")
{
	VarParameter::State parState = VarParameter::Dynamic;

	if(!isParam)
	{
		parName = defaultParName;
		parState = VarParameter::Fixed;
	}

	VarParameter* par = getVarParam(config, parName, parState);

	int inc = ind1<ind2 ? 1 : -1;

	for(int i = ind1; i != ind2+inc; i+=inc)
	{
		for(int j = i+inc; j != ind2+inc; j+=inc)
		{
			HeisenbergQOp *op = new HeisenbergQOp(i, j, config->numberOfSpins, J, "heis");
			StoreLocator<QOperator>::GetStore()->Add(op); // add new operators to store

			vector<Tensor*> comp = op->GetComponents(0);

			par->AddTensor(comp);

			config->operators.push_back(op);
		}
	}
}


//----------------------------------------------------------------------------------------------------
void HeisOpParser::createSingleOp(GlobalConfig *config, int ind1, int ind2, double J, bool isParam=false, string parName="")
{
	VarParameter::State parState = VarParameter::Dynamic;

	if(!isParam)
	{
		parName = defaultParName;
		parState = VarParameter::Fixed;
	}

	VarParameter* par = getVarParam(config, parName, parState);

	HeisenbergQOp *op = new HeisenbergQOp(ind1, ind2, config->numberOfSpins, J, "heis");
	StoreLocator<QOperator>::GetStore()->Add(op); // add new operators to store

	vector<Tensor*> comp = op->GetComponents(0);

	par->AddTensor(comp);

	config->operators.push_back(op);
}


}


