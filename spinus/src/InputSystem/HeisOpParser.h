/*
 * HeisOpParser.h
 *
 *  Created on: Sep 30, 2015
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_HEISOPPARSER_H_
#define INPUTSYSTEM_HEISOPPARSER_H_

#include "OperatorParser.h"
#include "../HeisenbergQOp.h"

namespace sps
{

class HeisOpParser: public sps::OperatorParser
{
public:
	HeisOpParser(string keyWord):
		OperatorParser(keyWord),
		fullConn("full"),
		chainConn("chain"),
		minRecLength(5)
		{}

	virtual ~HeisOpParser(){}


	virtual RecordInfo readLineRecord(GlobalConfig *config, RecordInfo inpinfo);
	virtual RecordInfo readBlockRecord(GlobalConfig *config, RecordInfo inpinfo){ return RecordInfo{RecordType::ReadError}; };

	void createChainConnectedOp(GlobalConfig *config, int ind1, int ind2, double J, bool isParam, string parName);
	void createFullConnectedOP(GlobalConfig *config, int ind1, int ind2, double J, bool isParam, string parName);
	void createSingleOp(GlobalConfig *config, int ind1, int ind2, double J, bool isParam, string parName);

private:
	string fullConn, chainConn;
	int minRecLength;
};

}


#endif /* INPUTSYSTEM_HEISOPPARSER_H_ */
