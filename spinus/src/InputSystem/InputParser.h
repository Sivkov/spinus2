/*
 * InputParser.h
 *
 *  Created on: Sep 10, 2015
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_INPUTPARSER_H_
#define INPUTSYSTEM_INPUTPARSER_H_


#include <string>
#include <vector>

#include "Parser.h"
#include "OperatorParser.h"
#include "HeisOpParser.h"
#include "AnisOpParser.h"
#include "ZeemanOpParser.h"
#include "HamiltonianParser.h"
#include "ParamParser.h"
#include "ObservablesParser.h"
#include "FileParDataLoader.h"
#include "StepParDataLoader.h"
#include "GaussParDataLoader.h"
#include "DensityMatrixParser.h"

using namespace std;

namespace sps
{


class InputParser//: public Parser
{
public:

	InputParser():
		comment('#'),
		delims("{};")
	{}


	vector<string> ReadScriptFile(string fileName);
	string GetScriptString();
	vector<string> GetScriptTokens();

	void LoadConfigFromFile(GlobalConfig *config, string fileName);
	
	//virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop);
	//virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start){ return Parser::Parse(config, scriptTokens,start);}
	//virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens){ return Parser::Parse(config, scriptTokens);}

protected:



	vector<string> scriptTokens;
	string scriptString;
	char comment;
	string delimRegEx;
	string delims;

};


}

/*

Operators
{


Heis
{
{1 2 1.0}
{2 3 J1}
{3 7 2.0 chain}
{7 10 alltoall}
}


Heis
{
1 2 1.0;
2 3 J1;
3 7 2.0 chain;
7 10 alltoall;
}

Heis { 10 11 0.1; }

Heis 11 12 J2;

Heis 13 20 chain;

Zeemann
{
1 B;

}

}

<Operators
<Heis
<1 2 0.1/>
<2 3 0.2/>
<3 6 0.4 chain/>
/>
/>




 */

#endif /* INPUTSYSTEM_INPUTPARSER_H_ */
