/*
 * ObservablesParser.cpp
 *
 *  Created on: Jan 8, 2016
 *      Author: isivkov
 */

#include "ObservablesParser.h"
#include <iostream>

namespace sps
{
RecordInfo ObservablesParser::Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	if(config->numberOfSpins == 0 || config->spins.size()==0)
	{
		throw SpinException("Observables parser: error, spin array is not set ");
	}

	RecordInfo rinfo = findRecordByKeyWord(keyWord, scriptTokens, start, stop);

	if( rinfo.type == RecordType::NotFound )
	{
		return rinfo;
	}

	if( rinfo.type != RecordType::Block )
	{
		throw SpinException("Observables record is not a block");
	}

	if(hamParser==nullptr)
	{
		throw SpinException("Hamiltonian parser is not set in Observables parser");
	}

	vector<string>::iterator startit = rinfo.out_start+2;
	vector<string>::iterator endit = rinfo.out_end;

	RecordInfo blockri = readBlock(scriptTokens,startit,endit);

	// небольшой костыль, handicap code
	while(blockri.type != RecordType::NotFound)
	{
		// set parameters of Hamiltonian Parser to make him to read found block
		hamParser->SetDefaultParName(blockri.name); // will create parameter with this name
		hamParser->SetKeyWord(blockri.name); // will parse block with this name

		//std::cout<<"name "<<blockri.name<<endl;
		// HamParser will store there observables VarParameters
		GlobalConfig tmpcfg;
		tmpcfg.numberOfSpins = config->numberOfSpins;
		tmpcfg.spins = config->spins;

		//parse block
		hamParser->Parse(&tmpcfg,scriptTokens,blockri.out_start,blockri.out_end);

		//get parameters and put them in right place
		//TODO valgrid tells here is a memory leak
		if(tmpcfg.parameters.size() !=0 )
		{
			vector<VarParameter*> tmpv;
			tmpv.swap(tmpcfg.parameters);
			config->observables.insert(config->observables.end(),tmpv.begin(),tmpv.end());
		}

		// next search will start with current end of block
		blockri = readBlock(scriptTokens,blockri.out_end,endit);
	}

	return rinfo;
}

}
