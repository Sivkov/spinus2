/*
 * ObservablesParser.h
 *
 *  Created on: Jan 8, 2016
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_OBSERVABLESPARSER_H_
#define INPUTSYSTEM_OBSERVABLESPARSER_H_

#include "OperatorParser.h"
#include "AnisOpParser.h"
#include "HeisOpParser.h"
#include "ZeemanOpParser.h"
#include "HamiltonianParser.h"

namespace sps
{

class ObservablesParser : public Parser
{
public:
	ObservablesParser(string keyWord, HamiltonianParser *hamp = nullptr): Parser(keyWord), hamParser(hamp) {};

	void SetHamiltonianParser(HamiltonianParser *hamp){hamParser = hamp;};

	virtual ~ObservablesParser(){};

	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop);
	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start){ return Parser::Parse(config, scriptTokens,start);}
	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens){ return Parser::Parse(config, scriptTokens);}

protected:
	HamiltonianParser *hamParser;

};

}


#endif /* INPUTSYSTEM_OBSERVABLESPARSER_H_ */
