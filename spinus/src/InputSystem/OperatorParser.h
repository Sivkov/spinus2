/*
 * OperatorsParser.h
 *
 *  Created on: Sep 30, 2015
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_OPERATORPARSER_H_
#define INPUTSYSTEM_OPERATORPARSER_H_

#include "Parser.h"


namespace sps
{


class OperatorParser: public Parser
{
public:
	OperatorParser(string keyWord): Parser(keyWord), defaultParName("main"){}
	//OperatorsParser(): Parser(){}

	string GetDefaultParName(){ return defaultParName; }
	void SetDefaultParName(string name){ defaultParName = name; }

	virtual ~OperatorParser(){}

	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop);
	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start){ return Parser::Parse(config, scriptTokens,start);}
	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens){ return Parser::Parse(config, scriptTokens);}

	// sons define this. read data from block or line record. inpinfo should contain information about it
	virtual RecordInfo readLineRecord(GlobalConfig *config, RecordInfo inpinfo)=0;
	virtual RecordInfo readBlockRecord(GlobalConfig *config, RecordInfo inpinfo)=0;



	// helping functions
	// get existing ar parameter by name or create param with specified name and state
	VarParameter* getVarParam(GlobalConfig *config, string parName, VarParameter::State parState);


protected:
	string defaultParName;

};

}


#endif /* INPUTSYSTEM_OPERATORPARSER_H_ */
