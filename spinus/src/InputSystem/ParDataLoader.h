/*
 * ParDataParser.h
 *
 *  Created on: Jan 15, 2016
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_PARDATALOADER_H_
#define INPUTSYSTEM_PARDATALOADER_H_

#include "Parser.h"

namespace sps
{

class ParDataLoader
{
public:
	ParDataLoader(string keyWord){ parser = new Parser(keyWord);}

	virtual ~ParDataLoader(){delete parser;}

	string GetKeyWord(){return parser->GetKeyWord();}

	virtual RecordInfo LoadParData(VarParameter *par, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)=0;

protected:
	Parser* GetParser(){return parser;}

private:
	Parser *parser;
};

}


#endif /* INPUTSYSTEM_PARDATALOADER_H_ */
