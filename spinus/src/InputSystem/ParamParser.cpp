/*
 * ParamParser.cpp
 *
 *  Created on: Oct 6, 2015
 *      Author: isivkov
 */
#include <algorithm>
#include <fstream>

#include "ParamParser.h"

namespace sps
{

ParamParser::ParamParser(string keyWord):
	Parser(keyWord)
{

}



RecordInfo ParamParser::Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	if(config->parameters.size()==0)
		throw SpinException("ParamParser: error, operators are, probably, not specified ");


	// read keyWord
	RecordInfo rinfo = findRecordByKeyWord(keyWord, scriptTokens, start, stop);

	//get non Fixed Parameters
	vector<VarParameter*> nonfix;
	std::copy_if(config->parameters.begin(), config->parameters.end(), back_inserter(nonfix),
			[](VarParameter* par) { return par->GetState() != VarParameter::Fixed; });

	// check results
	if(nonfix.size()==0)
	{
		return rinfo;
	}

	if(rinfo.type == RecordType::NotFound)
		throw SpinException("ParamParser: error, parameters are, probably, not specified");


	if( rinfo.type != RecordType::Block )
		throw SpinException("Parameters record is not a block");


	// now we can work
	for(auto par: nonfix)
	{
		string datatype;
		RecordInfo parri =readVal(datatype,par->GetName(), scriptTokens, rinfo.out_start, rinfo.out_end);

		if(parri.type != RecordType::Line)
			throw SpinException("Cannot find data for parameter '" + par->GetName() + "'");

		ParDataLoader *pl = nullptr;

		try
		{
			pl = loaders.at(datatype);
		}
		catch(std::exception &ex)
		{
			throw SpinException("Not known type of parameter data '" + datatype + "'");
		}

		pl->LoadParData(par,scriptTokens,parri.out_start,parri.out_end);
	}

	return rinfo;
}



void ParamParser::AddParDataLoader(ParDataLoader* loader)
{
	if(loader==nullptr) throw SpinException("Param data loader is NULL");
	loaders[loader->GetKeyWord()]=loader;
}

}
