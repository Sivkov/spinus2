/*
 * ParamParser.h
 *
 *  Created on: Oct 6, 2015
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_PARAMPARSER_H_
#define INPUTSYSTEM_PARAMPARSER_H_

#include <map>


#include "OperatorParser.h"
#include "AnisOpParser.h"
#include "HeisOpParser.h"
#include "ZeemanOpParser.h"

#include "ParDataLoader.h"


using namespace std;

namespace sps
{

class ParamParser : public Parser
{
public:
	ParamParser(string keyWord);

	virtual ~ParamParser(){};

	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop);
	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start){ return Parser::Parse(config, scriptTokens,start);}
	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens){ return Parser::Parse(config, scriptTokens);}

	void AddParDataLoader(ParDataLoader* loader);
protected:

	map<string,ParDataLoader*> loaders;
};

}


#endif /* INPUTSYSTEM_PARAMPARSER_H_ */
