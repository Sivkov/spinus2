/*
 * Parser.h
 *
 *  Created on: Sep 25, 2015
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_PARSER_H_
#define INPUTSYSTEM_PARSER_H_


#include <string>
#include <vector>

#include "../helper.h"
#include "../SpinException.h"
#include "../GlobalConfig.h"

using namespace std;

namespace sps
{

////////////////////////////////////////////////////////////////////////////////////////
enum class RecordType
{
	ReadError,
	NotFound,
	Line,
	Block
};


////////////////////////////////////////////////////////////////////////////////////////
struct RecordInfo
{
	RecordType type;
	vector<string>::iterator out_start;
	vector<string>::iterator out_end;
	string name;
};


////////////////////////////////////////////////////////////////////////////////////////
class Parser
{
public:

	Parser(string keyWord=""):
		keyWord(keyWord),
		lineCloseChar(";"),
		blockOpenChar("{"),
		blockCloseChar("}"),
		delimiters("{};")
	{}

	virtual ~Parser(){}

	string GetKeyWord() { return keyWord; }
	void SetKeyWord(string &kw) { keyWord = kw;}

	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
	{
		return RecordInfo{RecordType::ReadError};
	}

	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start)
	{
		return Parse(config,scriptTokens,start, scriptTokens.end());
	}

	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens )
	{
		return Parse(config,scriptTokens,scriptTokens.begin());
	}


	// read number after keyWord. If there are not number, then returns ReadError
	template<typename type>
	RecordInfo readVal(type &val, string keyWord, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator end);

	template<typename type>
	RecordInfo readVal(type &val, string keyWord, vector<string>& scriptTokens, vector<string>::iterator start);

	template<typename type>
	RecordInfo readVal(type &val, string keyWord, vector<string>& scriptTokens);

	template<typename type>
	bool readParamFromToken(string &token, type &par );

	template<typename type>
	void readValFromToken(string &token, type &val );

	// read number array after keyword until line close char from record with type Line.
	// Record with Block type returns RecordType::ReadError
	// if there are not numbers, then returns ReadError
	template<typename type>
	RecordInfo readArray(vector<type> &vals, string keyWord, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop);

	template<typename type>
	RecordInfo readArray(vector<type> &vals, string keyWord, vector<string>& scriptTokens, vector<string>::iterator start);

	template<typename type>
	RecordInfo readArray(vector<type> &vals, string keyWord, vector<string>& scriptTokens);


	// return info from found keyword to close char (line or block). RecordType is Line or Block, depending on the found result
	RecordInfo findRecordByKeyWord(string keyWord, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop);
	RecordInfo findRecordByKeyWord(string keyWord, vector<string>& scriptTokens, vector<string>::iterator start);
	RecordInfo findRecordByKeyWord(string keyWord, vector<string>& scriptTokens);

	// return info about tokens from start to close char (line or block). RecordType is Line
	RecordInfo readTillLineCloseChar(vector<string>& scriptTokens, vector<string>::iterator start);
	RecordInfo readBlock(vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop);



protected:

	string keyWord;

	string lineCloseChar;
	string blockOpenChar;
	string blockCloseChar;
	string delimiters;

};


//----------------------------------------------------------------------------------------------------
template<typename type>
inline void Parser::readValFromToken(string &token, type &val )
{
	try
	{
		val = stonum<type>(token);
	}
	catch(exception &ex) // bugaga
	{
		throw SpinException("Parser: error, cannot read parameter : ... '" + token + "' ...");
	}
}



//----------------------------------------------------------------------------------------------------
template<typename type>
inline bool Parser::readParamFromToken(string &token, type &par )
{
	try
	{
		par = stonum<type>(token);
	}
	catch(invalid_argument &ex) // ok, it is parameter
	{
		if( token.find_first_of(delimiters) != string::npos ) // special case if user is stupid :-)
		{
			throw SpinException("HeisOpParser: error, Heisenberg operator is wrong: ... '" + token + "' ...");
		}

		par = 1.0;

		return true;
	}

	return false;
}

//----------------------------------------------------------------------------------------------------
template<>
inline RecordInfo Parser::readVal(string &val, string keyWord, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	RecordInfo rinfo = findRecordByKeyWord(keyWord,scriptTokens, start, stop);

	if(rinfo.type == RecordType::NotFound)
		return rinfo;

	vector<string>::iterator it = rinfo.out_start;

	val = (*(++it));

	return rinfo;
}

//----------------------------------------------------------------------------------------------------
template<typename type>
inline RecordInfo Parser::readVal(type &val, string keyWord, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	RecordInfo rinfo = findRecordByKeyWord(keyWord,scriptTokens, start, stop);

	if(rinfo.type == RecordType::NotFound)
		return rinfo;

	vector<string>::iterator it = rinfo.out_start;

	try
	{
		val = stonum<type>(*(++it));
	}
	catch(invalid_argument &ex)
	{
		rinfo.type = RecordType::ReadError;
		throw SpinException("reading value, error with '" + keyWord + "' , '" + *it +"' is not a number");
		return rinfo;
	}


	return rinfo;
}


//----------------------------------------------------------------------------------------------------
template<typename type>
inline RecordInfo Parser::readVal(type &val, string keyWord, vector<string>& scriptTokens, vector<string>::iterator start)
{
	return readVal(val,keyWord, scriptTokens, start, scriptTokens.end());;
}


//----------------------------------------------------------------------------------------------------
template<typename type>
inline RecordInfo Parser::readVal(type &val, string keyWord, vector<string>& scriptTokens)
{
	return readVal(val,keyWord, scriptTokens, scriptTokens.begin());
}


//----------------------------------------------------------------------------------------------------
template<typename type>
inline RecordInfo Parser::readArray(vector<type> &vals, string keyWord, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	RecordInfo rinfo = findRecordByKeyWord(keyWord,scriptTokens, start, stop);

	if(rinfo.type == RecordType::NotFound)
	{
		return rinfo;
	}

	if(rinfo.type == RecordType::Block)
	{
		throw SpinException("reading array, error  with '" + keyWord + "' , '  wrong format of array");
		return rinfo;
	}

	vector<string>::iterator i=rinfo.out_start+1;

	while( i != rinfo.out_end - 1) // -1 because last allowed symbol is ";"
	{
		type val=0;

		try
		{
			val = stonum<type>(*i);
		}
		catch(invalid_argument &ex)
		{
			rinfo.type = RecordType::ReadError;
			throw SpinException("reading array, error with '" + keyWord + "' , '" + *i +"' is not a number");
			return rinfo;
		}

		vals.push_back(val);

		++i;
	}

	return rinfo;
}


//----------------------------------------------------------------------------------------------------
template<>
inline RecordInfo Parser::readArray<string>(vector<string> &vals, string keyWord, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	RecordInfo rinfo = findRecordByKeyWord(keyWord,scriptTokens, start, stop);

	if(rinfo.type == RecordType::NotFound)
	{
		return rinfo;
	}

	if(rinfo.type == RecordType::Block)
	{
		throw SpinException("reading array, error  with '" + keyWord + "' , '  wrong format of array");
		return rinfo;
	}

	vals = vector<string>(rinfo.out_start+1,rinfo.out_end - 1);

	return rinfo;
}

//----------------------------------------------------------------------------------------------------
template<typename type>
inline RecordInfo Parser::readArray(vector<type> &vals, string keyWord, vector<string>& scriptTokens, vector<string>::iterator start)
{
	return readArray(vals, keyWord, scriptTokens, start, scriptTokens.end());
}


//----------------------------------------------------------------------------------------------------
template<typename type>
inline RecordInfo Parser::readArray(vector<type> &vals, string keyWord, vector<string>& scriptTokens)
{
	return readArray(vals, keyWord, scriptTokens, scriptTokens.begin());
}

}


#endif /* INPUTSYSTEM_PARSER_H_ */
