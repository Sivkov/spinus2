/*
 * ZeemanOpParser.cpp
 *
 *  Created on: Oct 3, 2015
 *      Author: isivkov
 */



#include "ZeemanOpParser.h"

namespace sps
{


//----------------------------------------------------------------------------------------------------
RecordInfo ZeemanOpParser::readLineRecord(GlobalConfig *config, RecordInfo inpinfo)
{
	vector<string>::iterator it = inpinfo.out_start;

	if(inpinfo.type == RecordType::NotFound || inpinfo.type == RecordType::ReadError)
		return inpinfo;

	// read operator record to measure size (may be it is stupid)
	vector<string> subv(inpinfo.out_start,inpinfo.out_end);

	if( (int)subv.size() < minRecLength ) // because minimum format is  'heis 0 1 J;'
	{
		throw SpinException("ZeemanOpParser: error in Zeemain operator, to few parameters: ... '" + *it + "' ...");
	}

	int ind=0;
	int ind2=0;
	double B[3]= {1.0,1.0,1.0};
	string par[3];
	bool isPar[3]={false,false,false};

	// read spin index
	readValFromToken<int>(*(++it), ind);

	if( (int)subv.size()==minRecLength+1 ) // check second index, which means that we need to create several operators
	{
		readValFromToken<int>(*(++it), ind2);

		if(ind == ind2 || ind < 0 || ind2 < 0)
		{
			throw SpinException("ZeemanOpParser: error in Zeemain operator, spin indices are wrong: ... '" + *it + "' ...");
		}
	}

	// try to read parameterized parameters :)
	// if it is number - fine, if not - we need to make parameterized operator
	for(int i=0; i<3; i++)
	{
		par[i]=*(++it);
		isPar[i] = readParamFromToken<double>(par[i],B[i]);
	}

	// check if 4th parameter exists
	++it;

	if( (int)subv.size()==minRecLength && ((*it)==lineCloseChar || (*it)==blockCloseChar) ) // create only one operator
	{
		createSingleOp(config, ind, B, isPar, par);
		return inpinfo;
	}

	if( (int)subv.size()==minRecLength+1 && ((*it)==lineCloseChar || (*it)==blockCloseChar)) // range of spins
	{
		createRangeSpinsOP(config, ind, ind2, B, isPar, par);
		return inpinfo;
	}

	// if we are here, then something seriously wrong
	throw SpinException("AnisOpParser: error, Anisotropy operator is wrong, something missed: ... '" + *(it-2) + " " + *(it-1) + *it + "' ...");
	return inpinfo;
}



//----------------------------------------------------------------------------------------------------
void ZeemanOpParser::createRangeSpinsOP(GlobalConfig *config, int ind, int ind2, double B[3], bool isPar[3], string par[3])
{
	VarParameter::State parState[3] = {VarParameter::Dynamic, VarParameter::Dynamic, VarParameter::Dynamic};

	for(int i=0; i< 3; i++)
	{
		if(!isPar[i])
		{
			par[i] = defaultParName;
			parState[i] = VarParameter::Fixed;
		}
	}

	VarParameter* pB[3];
	for(int i=0;i<3;i++) pB[i] = getVarParam(config, par[i], parState[i]);

	int inc = ind<ind2 ? 1 : -1;

	for(int i = ind; i != ind2+inc; i+=inc)
	{
		ZeemanQOp *op = new ZeemanQOp(i, config->numberOfSpins, B[0] * factor, B[1] * factor, B[2] * factor, "zeeman" );
		StoreLocator<QOperator>::GetStore()->Add(op); // add new operators to store

		config->operators.push_back(op);

		for(int c = 0; c< 3 ; c++)
		{
			vector<Tensor*> comp = op->GetComponents(c);
			pB[c]->AddTensor(comp);
		}
	}
}


//----------------------------------------------------------------------------------------------------
void ZeemanOpParser::createSingleOp(GlobalConfig *config, int ind, double B[3], bool isPar[3], string par[3])
{
	VarParameter::State parState[3] = {VarParameter::Dynamic, VarParameter::Dynamic, VarParameter::Dynamic};

	for(int i=0; i< 3; i++)
	{
		if(!isPar[i])
		{
			par[i] = defaultParName;
			parState[i] = VarParameter::Fixed;
		}
	}

	VarParameter* pB[3];
	for(int i=0;i<3;i++) pB[i] = getVarParam(config, par[i], parState[i]);


	ZeemanQOp *op = new ZeemanQOp(ind, config->numberOfSpins, B[0] * factor, B[1] * factor, B[2] * factor, "zeeman");
	StoreLocator<QOperator>::GetStore()->Add(op); // add new operators to store

	config->operators.push_back(op);

	for(int c = 0; c< 3 ; c++)
	{
		vector<Tensor*> comp = op->GetComponents(c);
		pB[c]->AddTensor(comp);
	}

}

}
