/*
 * ZeemanOpParser.h
 *
 *  Created on: Oct 2, 2015
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_ZEEMANOPPARSER_H_
#define INPUTSYSTEM_ZEEMANOPPARSER_H_

#include "OperatorParser.h"
#include "../ZeemanQOp.h"

namespace sps
{

class ZeemanOpParser: public sps::OperatorParser
{
public:
	ZeemanOpParser(string keyWord):
		OperatorParser(keyWord),
		rangeSpins("range"),
		minRecLength(6),
		factor(2.0 * 5.8 * 0.01)
		{}

	virtual ~ZeemanOpParser(){}


	virtual RecordInfo readLineRecord(GlobalConfig *config, RecordInfo inpinfo);
	virtual RecordInfo readBlockRecord(GlobalConfig *config, RecordInfo inpinfo){ return RecordInfo{RecordType::ReadError}; };

	void createRangeSpinsOP(GlobalConfig *config, int ind, int ind2, double B[3], bool isPar[3], string par[3]);
	void createSingleOp(GlobalConfig *config, int ind1, double B[3], bool isPar[3], string par[3]);

private:
	string rangeSpins;
	int minRecLength;
	double factor;
};

}


#endif /* INPUTSYSTEM_ZEEMANOPPARSER_H_ */
