/*
 * QStateManager.h
 *
 *  Created on: Dec 5, 2014
 *      Author: isivkov
 */

#ifndef JJSTATE_H_
#define JJSTATE_H_

#include <vector>
#include <omp.h>
//#include <Eigen/Dense>

#include "JJStateNode.h"
#include "MatrixUnsafe.h"

using namespace std;
//using Eigen::MatrixXd;

namespace sps
{


class JJState
{
public:
	JJState(int initTwoJ);
	~JJState();

	void BuildBasis();
	void PlusJ(int twoAddJ);
	void PlusJ(std::vector<int>& twoJList);

	long int GetNumJStates() const;
	long int GetBasisDimension() const;

	const std::vector<int>& getInitTwoJ() const;

	std::vector<const JJStateNode*> GetLastNodes() const;

	const unsafe::Matrix1D<int>& GetFullBasis() const;

	const unsafe::Matrix1D<int>& GetJJBasis() const;

	const vector<long int>& GetJtoMBasisConnection() const;

	bool IsBasisBuilt() const
	{
		return isBasisBuilt;
	}

	int GetNumParticles() const
	{
		return numParticles;
	}

private:

	// Sum new J to pointed state node, build subnodes of the node
	void plusJ(JJStateNode* stateNode, int twoAddJ);

	void extractLastJJNodes(JJStateNode *pState, std::vector<JJStateNode*> &store);

	// State tree, built during J summation
	JJStateNode *stateTree;

	// pointers for end nodes of the State Tree for basis building
	std::vector<JJStateNode*> lastJJNodes;

	// for accounting of all pointers to delete them at the end
	std::vector<JJStateNode*> statesStore;

	// contain full JM basis for anisotropic calculation in general, each row of the matrix contains 2 elements <number of jj conf> and <M projection>
	unsafe::Matrix1D<int> fullBasis;

	// added init J for summation
	std::vector<int> initJ;

	// vector of jj configurations
	unsafe::Matrix1D<int> jjBasis;

	// connection from jjBasis to fullBasis
	vector<long int> mBasis;

	// number of all states in JM configuration
	long int basisDimension;

	// number of JJ (J1 J2 [J12] ... J) configurations
	long int numJStates;

	bool isBasisBuilt;

	int numParticles;
};


}


#endif /* JJSTATE_H_ */
