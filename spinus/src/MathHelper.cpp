/*
 * MathHelper.cpp
 *
 *  Created on: Sep 20, 2015
 *      Author: isivkov
 */

#include "MathHelper.h"
#include <iostream>
namespace sps
{


#if defined(__MACH__) && !defined(CLOCK_REALTIME)
#include <sys/time.h>
#define CLOCK_REALTIME 0
// clock_gettime is not implemented on older versions of OS X (< 10.12).
// If implemented, CLOCK_REALTIME will have already been defined.
int clock_gettime(int /*clk_id*/, struct timespec* t) {
    struct timeval now;
    int rv = gettimeofday(&now, NULL);
    if (rv) return rv;
    t->tv_sec  = now.tv_sec;
    t->tv_nsec = now.tv_usec * 1000;
    return 0;
}
#endif



//---------------------------------------------------------------------------------------------------------------
double GetTick(char units) {

	double del=1000.;

	switch(units)
	{
	case 's':del=1000000000;break;
	case 'm':del=1000000;break;
	default:break;
	}

    struct timespec ts;
    double time;

    clock_gettime( CLOCK_REALTIME, &ts );

    time  = (double)ts.tv_nsec/del ;
    time += ts.tv_sec * 1000000000./del;

    return time;
}


//---------------------------------------------------------------------------------------------------------------
// z= a*x+y
void sps_zaxpy(const MKL_INT n, const MKL_Complex16 a, const MKL_Complex16 *x,  MKL_Complex16 *y, MKL_Complex16 *z)
{
#pragma omp parallel for //simd vectorlength(2)
	for(int j=0;j<n;j++)
	{
		z[j]=a*x[j]+y[j];
	}
}



//---------------------------------------------------------------------------------------------------------------
void sps_zcoovmv(MKL_INT n, MKL_Complex16 *vec, MKL_Complex16 *tmpvec, MKL_INT nnz, MKL_Complex16 *val,
		MKL_INT *indI, MKL_INT *indJ, MKL_Complex16 *res )
{
	MKL_Complex16 alpha=1.0,beta=0.0;

	mkl_zcoomv("N", &n, &n, &alpha, "HLNCAA", val, indI, indJ, &nnz, vec, &beta, tmpvec);

	cblas_zdotc_sub(n, vec, 1, tmpvec, 1, res);
}


//---------------------------------------------------------------------------------------------------------------
double factorial(int n)
{
	return (n == 1 || n == 0) ? 1.0 : factorial(n - 1) * n;
}

//---------------------------------------------------------------------------------------------------------------
double factorial(int n, int m)
{
	return (n <= 1 || n <=m) ? 1.0 : factorial(n - 1, m) * n;
}

//---------------------------------------------------------------------------------------------------------------
double calcVonNeumanEntropy(MKL_Complex16* densityMatrix, int dim, MKL_Complex16* tmpVec)
{
		Eigen::Map<sps::MatrixZ> mat(densityMatrix,dim,dim);
		Eigen::Map<sps::VectorZ> vec(tmpVec,dim);

		vec = mat.eigenvalues();
//std::cout<<"-------------\n"<<vec<<std::endl;
		double entropy = 0.0;

		for(int i=0; i < dim; i++)
		{
			entropy -= std::abs(tmpVec[i]) * std::log(std::abs(tmpVec[i])) / Log2;
		}
//		std::cout<<"entr "<<entropy<<std::endl;
		return entropy;
}


}
