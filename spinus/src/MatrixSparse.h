/*
 * MatrixSparse.h
 *
 *  Created on: Sep 1, 2015
 *      Author: isivkov
 *
 *      Simple class, which only stores data
 *      May be it will be extended
 */

#ifndef MATRIXSPARSE_H_
#define MATRIXSPARSE_H_

#include <vector>

using namespace std;

namespace sps
{

enum class SparseType
{
	COO,
	CSR,
	CSR3,
	CSC,
	CSC3
};

template<class type>
class MatrixSparse
{
public:


	MatrixSparse()
	:spType(SparseType::COO),
	 width(0),
	 height(0),
	 numEl(0){}

	SparseType GetType()
	{
		return spType;
	}

	void SetType(SparseType sptype)
	{
		spType=sptype;
	}

	// vectors
	vector<type>* GetValuesVec()
	{
		return &values;
	}

	vector<int>* GetMainIndsVec()
	{
		return &mainInds;
	}


	vector<int>* GetPBVec()
	{
		return &pB;
	}

	vector<int>* GetPEVec()
	{
		return &pE;
	}

	// Raw
	type* GetValuesRaw()
	{
		return values.data();
	}

	int* GetMainIndsRaw()
	{
		return mainInds.data();
	}

	int* GetPBRaw()
	{
		return pB.data();
	}

	int* GetPERaw()
	{
		return pE.data();
	}

	//sizes
	int GetValuesSize()
	{
		return values.size();
	}

	int GetMainIndsSize()
	{
		return mainInds.size();
	}

	int GetPBSize()
	{
		return pB.size();
	}

	int GetPESize()
	{
		return pE.size();
	}

	int GetNumElem()
	{
		return numEl;
	}

	void COOFromDense(type *data, int width, int height);
	void COOFromDenseTr(type *data, int dimension);

protected:

	vector<type> values;
	vector<int> mainInds, pB,pE;

	SparseType spType;

	int numEl, width, height;
};


template<class type>
void MatrixSparse<type>::COOFromDense(type *data, int width, int height)
{
	spType = SparseType::COO;

	this->width = width;
	this->height = height;

	unsigned int chunkSize=2;

	if(width+height >=8)
		chunkSize= (width+height) / 4;

	int denseSize = width * height;
	int sparseSize=0;
	int inddns=0 , indsp=0;

	int i=0,j=0;

	for(i=0;  i<width; i++)
	{
		for(j=0;  j<height; j++)
		{
			if(indsp >= values.size())
			{
				sparseSize += chunkSize;
				values.resize(sparseSize);
				mainInds.resize(sparseSize);
				pB.resize(sparseSize);
			}

			if( data[inddns] != 0 )
			{
				values[indsp] = data[inddns];
				mainInds[indsp] = i;
				pB[indsp] = j;
				indsp++;
			}

			inddns++;
		}
	}

	numEl = indsp;
}



template<class type>
void MatrixSparse<type>::COOFromDenseTr(type *data, int dimension)
{
	spType = SparseType::COO;

	this->width = dimension;
	this->height = dimension;

	unsigned int chunkSize=2;

	if(width+height >=8)
		chunkSize= (width+height) / 4;

	int denseSize = width * height;
	int sparseSize=0;
	int inddns=0 , indsp=0;

	int i=0,j=0;

	for(i=0;  i<dimension; i++)
	{
		for(j=0;  j<=i; j++)
		{
			if(indsp >= values.size())
			{
				sparseSize += chunkSize;
				values.resize(sparseSize);
				mainInds.resize(sparseSize);
				pB.resize(sparseSize);
			}

			inddns = j+dimension*i;

			if( data[inddns] != (type)0 )
			{
				values[indsp] = data[inddns];
				mainInds[indsp] = i;
				pB[indsp] = j;
				indsp++;
			}
		}
	}

	numEl = indsp;
}

}


#endif /* MATRIXSPARSE_H_ */
