/*
 * PairTensor.h
 *
 *  Created on: Jun 2, 2015
 *      Author: isivkov
 */

#ifndef PAIRTENSOR_H_
#define PAIRTENSOR_H_

#include "Tensor.h"
#include "SpinException.h"

namespace sps
{

class PairTensor : public Tensor
{
public:
	PairTensor(int numS1=0,
			int numS2=1,
			int numTot=2,
			int twoJ1=1,
			int twoJ2=1,
			int twoJ=0,
			int twoM=0,
			MKL_Complex16 factor=1.0,
			std::string name = "None")
	{
		if(numS1 >= numTot || numS2 >= numTot)
			throw SpinException("Wrong tensor parameter: numS >= numTot");

		this->M=twoM;
		this->factor=factor;
		this->name = name;

		initTwoJ = std::vector<int>(numTot,0);
		addTwoJ = std::vector<int>(numTot,0);

		int min=0, minJ=0;
		int max=0, maxJ=0;

		if(numS1==numS2)
			throw SpinException("Pair tensor is wrong initialized, numS1==numS2");

		if(numS1 < numS2)
		{
			min=numS1;
			max=numS2;
			minJ=twoJ1;
			maxJ=twoJ2;
		}
		else
		{
			min=numS2;
			max=numS1;
			minJ=twoJ2;
			maxJ=twoJ1;
		}

		for(int i=0; i<numTot; i++)
		{
			if(i==min)
				initTwoJ[i] = minJ;

			if(i==max)
				initTwoJ[i] = maxJ;


			if( i >= min)
			{
				if( i < max )
					addTwoJ[i] = minJ;
				else
					addTwoJ[i] = twoJ;
			}
		}
	}
};
}



#endif /* PAIRTENSOR_H_ */
