/*
 * QOperator.h
 *
 *  Created on: Jun 2, 2015
 *      Author: isivkov
 */

#ifndef QOPERATOR_H_
#define QOPERATOR_H_

#include <vector>

#include "Tensor.h"
#include "StoreLocator.h"

namespace sps
{


class QOperator
{
public:
	QOperator():name("") {	}
	QOperator(string name):name(name) {	}

	virtual std::vector<Tensor*> GetComponents(int num)=0;

	virtual ~QOperator()
	{

	}


	std::vector<Tensor*> GetAllTensors()
	{
		return tensors;
	}

	std::string GetName() { return name; }

protected:
	std::vector<Tensor*> tensors;
	std::vector<MKL_Complex16> params;
	std::string name;
};

}


#endif /* QOPERATOR_H_ */
