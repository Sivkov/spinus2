/*
 * SDSolver.h
 *
 *  Created on: Jun 29, 2015
 *      Author: isivkov
 */

#ifndef SDSOLVER_H_
#define SDSOLVER_H_

#include <functional>
#include <vector>

namespace sps
{

using namespace std;

template<class DataType>
using RHSFunc = function<void(const double*, const vector<DataType>*, vector<DataType>*)> ;

template<class DataType>
class SDSolver
{
public:
	SDSolver():slvvec(nullptr)
	{	}

	virtual ~SDSolver(){};

	virtual void Solve()
	{	};

	void SetUserFunc(function<void(bool*)> &userF)
	{
		userFunc=userF;
	}

	void SetRHSCalcFunc(RHSFunc<DataType> &rhsF)
	{
		rhsFunc=rhsF;
	}

	void SetSlvVec(vector<DataType>* vec)
	{
		slvvec = vec;
	}

protected:
	function<void(void)> userFunc;
	RHSFunc<DataType> rhsFunc;
	vector<DataType>* slvvec;
};
}



#endif /* SDSOLVER_H_ */
