/*
 * SparseMatrixS.h
 *
 *  Created on: Sep 10, 2017
 *      Author: isivkov
 */

#ifndef SPINUS_SRC_SPARSEMATRIXS_H_
#define SPINUS_SRC_SPARSEMATRIXS_H_

#include <mkl.h>

#include <stdexcept>
    
#include <Eigen/Dense>
#include <Eigen/SparseCore>

#include <iostream>

namespace sps
{


template <typename _Scalar, int _Options = Eigen::ColMajor, typename _StorageIndex = int>
class SparseMatrixS : public Eigen::SparseMatrix<_Scalar, _Options, _StorageIndex>
{
public:
	using  Eigen::SparseMatrix< _Scalar,  _Options,  _StorageIndex> ::  SparseMatrix ;

	// performs *this += arg * mat. Works correct if all indices of mat2 exist in mat1
	// needs to be compressed before
	template<typename ArgT>
	void Add(Eigen::SparseMatrix<_Scalar, _Options, _StorageIndex>& mat, ArgT arg = 0.0)
	{
		_Scalar* vals1 = this->valuePtr();
		_StorageIndex* inner1 = this->innerIndexPtr();
		_StorageIndex* outer1 = this->outerIndexPtr();

		_Scalar* vals2 = mat.valuePtr();
		_StorageIndex* inner2 = mat.innerIndexPtr();
		_StorageIndex* outer2 = mat.outerIndexPtr();

		#pragma omp parallel for
		for(int iout = 0; iout < this->outerSize(); iout++)
		{
			_StorageIndex* innerLoc2 = &inner2[outer2[iout]];
			_Scalar* valsLoc2  = &vals2[outer2[iout]];
			int numNnz2 = outer2[iout + 1] - outer2[iout];

			_StorageIndex iinn2 = 0;
			for(int iinn1 = outer1[iout]; iinn1 < outer1[iout + 1] && iinn2 < numNnz2 ; iinn1++)
			{
				if(inner1[iinn1] == innerLoc2[iinn2])
				{
					vals1[iinn1] += arg*valsLoc2[iinn2];
					++iinn2;
				}
			}
		}
	}

	// needs to be compressed before
	void ZeroValues()
	{
		memset(this->valuePtr(), 0, this->nonZeros() * sizeof(_Scalar));
	}


	void trheMV(_Scalar alpha, _Scalar beta, _Scalar* vecx, _Scalar* vecy)
	{
		//std::cout<<"vm alpha "<<alpha<<" beta "<<beta<<std::endl;
		if(this->rows() != this->cols())
		{
			throw std::runtime_error("Error! Matrix is not square");
		}

		_StorageIndex n = this->rows();
		mkl_zcsrmv ("N", &n, &n, &alpha, "HLNCAA", this->valuePtr(),
				this->innerIndexPtr(), this->outerIndexPtr(), this->outerIndexPtr() + 1,
				vecx, &beta, vecy );
		//std::cout<<"."<<std::endl;
	}

	void trheMV(_Scalar alpha, _Scalar beta, _Scalar* vecx, _Scalar* vecy)
    const
    {
        trheMV(alpha, beta, vecx, vecy);
    }
    
    void trheMV(_Scalar* vecx, _Scalar* vecy)
	const
	{
		trheMV(1.0, 0.0, vecx, vecy);
	}
	
    void trheMV(_Scalar* vecx, _Scalar* vecy)
	{
		trheMV(1.0, 0.0, vecx, vecy);
	}

	_Scalar trheVMV(_Scalar alpha, _Scalar beta, _Scalar* vec, _Scalar* workVec)
	{
		trheMV(alpha, beta, vec, workVec);
		//std::cout<<"vmv alpha "<<alpha<<" beta "<<beta<<std::endl;
		Eigen::Map<Eigen::Matrix<_Scalar, Eigen::Dynamic, 1>> vecEigen(vec, this->rows());
		Eigen::Map<Eigen::Matrix<_Scalar, Eigen::Dynamic, 1>> workVecEigen(workVec, this->rows());
		return vecEigen.dot(workVecEigen);
		//std::cout<<" . "<<std::endl;
	}

	_Scalar trheVMV(_Scalar* vec, _Scalar* workVec)
	{
		return trheVMV(1.0, 0.0, vec, workVec);
	}
};

}

#endif /* SPINUS_SRC_SPARSEMATRIXS_H_ */
