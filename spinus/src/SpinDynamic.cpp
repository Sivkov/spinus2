/*
 * SpinDynamics.cpp
 *
 *  Created on: Jul 21, 2015
 *      Author: isivkov
 */



/*
 * SpinStatic.cpp
 *
 *  Created on: Jul 10, 2015
 *      Author: isivkov
 */

#include <iostream>
#include <iomanip>


#include "SpinDynamic.h"
#include "MatrixSparse.h"
#include "MathHelper.h"

#include <mkl_spblas.h>

#include <vector>

#include <arscomp.h>
#include "check_arpack.h"

//using namespace std;

namespace sps
{

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
SpinDynamic::SpinDynamic()
{ }


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
SpinDynamic::~SpinDynamic()
{

}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::SetJJState(JJState *jjs)
{
	jjState=jjs;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::SetHamBuilder(HamiltonianBuilder *hb)
{
	hamBuilder=hb;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::SetConfig(SpinDynamicConfig &config)
{
	this->config=config;
	configured=true;
	//numOutSpins = config.spinNumsForMagMom.size();
	damping = config.damping;
	timeStep = config.timeStep;
	stepsNum = config.timeEnd / timeStep;
	saveEverySteps = config.saveEveryPS / timeStep  ;
	if(saveEverySteps==0)	saveEverySteps = 1 ;
	numSaveSteps = stepsNum / saveEverySteps;
}




//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::Calc()
{
	if( !(hamBuilder->GetConfigured()) )
	{
		throw SpinException("Hamiltonian is not configured");
	}

	//-----------------------------------------------------------------
	// prepare
	//-----------------------------------------------------------------

	// param init, for quicker access
	paramInit();

	// base inits, allocation some arrays and others
	baseInit();

	initDM();


	cout<<"-----------------------------------"<<endl;

	cout<<"number of steps:\t"<< stepsNum<<endl;
	cout<<"save data every:\t"<< saveEverySteps<<" steps"<<endl;
	cout<<"num save steps:\t" << numSaveSteps<<endl;

	cout<<"-----------------------------------"<<endl;

	//find needed eigvals and/or std::vectors
	cout<<"Find eigenvals for first step"<<endl;

	saveInd=0;	
	calcTotalMatrixForTime(0.0);

	eigSolve();

	// stupidity
	calcTotalMatrixForTime(0.0);

	saveEnergy(0);
	calcMagMom(0);
	calcDM(0);

	cout<<"Start main loop"<<endl;

	// counter for output during calc
	int outEvery = stepsNum/100 + 1;
	int curCount=0;
	int percentDone=0;

	int saveTime=0;

	time=0.0;


	//-----------------------------------------------------------------
	// main loop
	//-----------------------------------------------------------------

	for(int k=0;k<stepsNum-1; k++)
	{
		parLoopIndex = k;

//		Eigen::Map<VectorZ> vecEigen(curEigVecs.data(), curEigVecs.size());
//
//		cout<< vecEigen <<endl;
//
//		cout<< totmat <<endl;
//
//		cout<<"energy "<<energy<<endl;
//		cin.get();
///////////////////////////////////////////////////////////////////////////////

		if(!calcStep())
			break;

		//save physical data
		if(saveTime == saveEverySteps && saveInd < numSaveSteps -1)
		{
			saveInd++;
			calcMagMom(saveInd);
			calcObservables(saveInd);
			saveEnergy(saveInd);
			calcDM(saveInd);

			saveTime=0;
		}
		saveTime++;

		// output amount of performed work
		curCount++;
		if(curCount == outEvery)
		{
			//normalize std::vector
			double norm = cblas_dznrm2 (basisDim, curEigVecs.data(), 1);
			percentDone++;
			cout << "Energy: "<<energy<<" meV , "<<percentDone<<"% done. wave function norm = "<<norm<<endl;
			curCount=0;
		}

		time+=timeStep;
	}

	if(!breakCalc)
	{
		cout<<"Calculation finished, output results"<<endl;

		outputMagMom();
		outputObserv();
		outputParams();
		outputDM();
	}

}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::saveEnergy(int saveInd)
{
	energies[saveInd] = energy;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::baseInit()
{
	//------------------------
	// base init
	//------------------------
	basisDim = jjState->GetBasisDimension();

	curEigVals.resize(basisDim);
	curEigVecs.resize(basisDim);

	// for Runge-Kutta
	rhs.resize(basisDim);
	k1.resize(basisDim);
	k2.resize(basisDim);
	k3.resize(basisDim);
	k4.resize(basisDim);
	tmp1.resize(basisDim);
	tmp2.resize(basisDim);
	tmp3.resize(basisDim);
	tmp4.resize(basisDim);

	// total matrix must contain all possible non-zeros
	// for that we add all the parameters matrices to that
	// to allocate needed memory inside
	totmat = SparseMatrixZ(basisDim,basisDim);
	for(auto& mat: hamBuilder->GetVarParams())
	{
		totmat += mat->GetMatrix();
	}

	// constants for Schroedinger equation
	dampCoef1 = ( - Im - damping) / ( PlanckConstPS * (1+ damping * damping));
	dampCoef2 = damping / (PlanckConstPS * (1+ damping * damping));
	imPlanckCoef = -Im/PlanckConstPS;

	// init for magneti moment calculation
	numOutSpins = hamBuilder->GetSpinMatrs().size();

	curMagMoms.resize(numOutSpins);
	magMoms   .resize(numSaveSteps * numOutSpins);

	energies.resize(numSaveSteps);

	// observables
	if(hamBuilder->GetObservMatrs().size()!=0)
	{
		observables.resize(hamBuilder->GetObservMatrs().size() * numSaveSteps);
		calcObserv=true;
	}

	// tmp vec to store dot product
	tmpvec2.resize(basisDim * std::max(numOutSpins, (int)hamBuilder->GetObservMatrs().size()));
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::paramInit()
{
	//TODO make good parameter system. This is shit
	// get params for loop
	vector<VarParameter*> &params = hamBuilder->GetVarParams();

	// basic arrays init
	curParams.resize(params.size());

	// loop for params
	for(int i = 0; i< params.size(); i++)
	{
		const std::vector<double>& pardata = params[i]->GetParData();

		if(maxParDataSize < pardata.size()) maxParDataSize = pardata.size();

		if( params[i]->GetArgStep() == 0)
		{
			params[i]->SetArgStep(timeStep);
		}
	}

}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::initDM()
{
	std::vector<DensityMatrixSingle*> &dmsv = *hamBuilder->GetSingleDMs();
	std::vector<DensityMatrixDouble*> &dmdv = *hamBuilder->GetDoubleDMs();

	// copy all matrices pointers in one array for easier access
	densityMatrices.insert(densityMatrices.end(),dmsv.begin(), dmsv.end());
	densityMatrices.insert(densityMatrices.end(),dmdv.begin(), dmdv.end());

	if( densityMatrices.size() != 0 )
	{
		calcEntropy = true;
	}
	else
	{
		return;
	}

	entropy.resize(densityMatrices.size()*numSaveSteps,0.0);

	curDMeigvals.resize(densityMatrices.size());
	curDMs.resize(densityMatrices.size());

	for(int i = 0; i< densityMatrices.size(); i++)
	{
		curDMs[i].resize(densityMatrices[i]->GetDimensions() * densityMatrices[i]->GetDimensions(),0.0);
		curDMeigvals[i].resize(densityMatrices[i]->GetDimensions(),0.0);
	}
}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::outputMagMom()
{
	// magmom file reopen and write head line
	std::ofstream magf(config.magMomOutFile);

	if(magf.is_open())
	{
		magf<<"Time\t";
		magf<<"Energy\t";

		for(auto n: hamBuilder->GetSpinNames())
		{
			magf<<n<<"\t";
		}

		magf<<endl;

		for(int i=0; i < numSaveSteps; i++)
		{
			magf << i * config.saveEveryPS << "\t";
			magf << energies[i] << "\t";

			for(int j=0; j < numOutSpins ; j++)
			{
				magf <<std::fixed<<std::setprecision(6)<< magMoms[i * numOutSpins  + j]<<"\t";
			}
			magf<<endl;
		}

		magf.close();
	}
	else
	{
		cout<<config.magMomOutFile + " file is not opened"<<endl;
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::outputObserv()
{
	int numObservMatrs = hamBuilder->GetObservMatrs().size();

	// magmom file reopen and write head line
	ofstream magf(config.observablesFile);

	if(magf.is_open())
	{
		// write first line
		magf<<"Time\t";

		for(auto n: hamBuilder->GetObservNames())
		{
			magf<<n<<"\t";
		}

		magf<<endl;

		for(int j=0; j<numSaveSteps; j++)
		{
			magf << j*config.saveEveryPS << "\t";

			for(int i=0;i<numObservMatrs; i++)
			{
				magf <<std::fixed<<std::setprecision(6)<<observables[j * numObservMatrs + i]<<"\t";
			}
		magf<<endl;
		}

		magf.close();
	}
	else
	{
		cout<<config.observablesFile + " file is not opened"<<endl;
	}
}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::outputDM()
{
	if( !calcEntropy )
	{
		return;
	}

	ofstream magf(config.entropyFile);
	if(magf.is_open())
	{
		// write first line
		magf<<"Time        ";

		for(auto m: densityMatrices)
		{
			magf<<"E_";
			for(auto i: m->GetSpinIndices()) magf<<i<<"_";
			if( m->GetSpinIndices().size() == 1 ) magf<<"        ";
			if( m->GetSpinIndices().size() == 2 ) magf<<"      ";
		}

		magf<<endl;

		for(int j=0; j<numSaveSteps; j++)
		{
			magf <<std::fixed<<std::setprecision(6)<< j*config.saveEveryPS << "    ";

			for(int i=0;i<densityMatrices.size(); i++)
			{
				magf <<std::fixed<<std::setprecision(6)<<entropy[j*densityMatrices.size()+i]<<"    ";
			}
		magf<<endl;
		}

		magf.close();
	}
	else
	{
		cout<<config.entropyFile + " file is not opened"<<endl;
	}
}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::outputParams()
{
	for(auto par: hamBuilder->GetVarParams())
	{
		if(par->GetState()==VarParameter::Fixed)
			continue;

		string pname = par->GetName();

		ofstream magf("_param_"+pname+".dat",std::ofstream::out);

		if(magf.is_open())
		{
			// write first line
			magf<<"Time\t"<<pname<<endl;

			for(int j=0; j<par->GetParData().size(); j++)
			{
				magf << j*par->GetArgStep() << "\t" << par->GetParData()[j] << endl;
			}

			magf.close();
		}
		else
		{
			cout<<"file is not opened"<<endl;
		}
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcParamsForStep(int step)
{
	if( step > maxParDataSize) return;

	vector<VarParameter*> &params = hamBuilder->GetVarParams();

	// setup parameters for this iteration
	for(int i = 0; i< params.size(); i++)
	{
		const std::vector<double>& pardata = params[i]->GetParData();

		int psize = pardata.size();

		if(psize > step)
		{
			curParams[i] = pardata[step];
		}


		if(psize==0 || params[i]->GetState() == VarParameter::Fixed)
		{
			curParams[i]=1.0;
		}
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcParamsForTime(double time)
{
	vector<VarParameter*> &params = hamBuilder->GetVarParams();

	// TODO optimization: all params in one array
	// setup parameters for this iteration
	for(size_t i = 0; i < params.size(); i++)
	{
		double parstep = params[i]->GetArgStep();
		int step = time / parstep;

		const std::vector<double>& pardata = params[i]->GetParData();

		int psize = pardata.size();

		// if check if we have no parameter array for current parameter
		if(psize==0 || params[i]->GetState() == VarParameter::Fixed)
		{
			curParams[i]=1.0;
			continue;
		}

		// else if we have array of parameters
		// calc current step
		double fractpart = time - step*parstep;

		// if we reached end of parameter array
		if(psize > step-1)
		{
			// interpolation
			curParams[i] = pardata[step] + (pardata[step+1]-pardata[step]) * fractpart/parstep;
		}
		else
		{
			curParams[i] = pardata[psize-1];
		}
	}
}

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcTotalMatrix()
{
	totmat.ZeroValues();

	std::vector<VarParameter*> &params = hamBuilder->GetVarParams();

	for(size_t i=0; i < curParams.size(); i++ )
	{
		totmat.Add(params[i]->GetMatrixUnsafe() , curParams[i]);
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcTotalMatrixForTime(double time)
{
	int step = time/timeStep;
	
	if(step > maxParDataSize) return;
	
	calcParamsForTime(time);
	calcTotalMatrix();
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcRHS(MKL_Complex16 *eigvecdata, MKL_Complex16 *rhsdata, MKL_Complex16 *tmpdata, long int size)
{
	if(damping != 0)
	{
		energy = totmat.trheVMV(eigvecdata, tmpdata).real();

		// copy eigvec to rhs, because rhs will be overwritten
		memcpy(rhsdata,eigvecdata, sizeof(MKL_Complex16) * size);

		// calc rhs
		totmat.trheMV(dampCoef1, energy * dampCoef2, eigvecdata, rhsdata);
	}
	else
	{
		totmat.trheMV(imPlanckCoef, 1.0, eigvecdata, rhsdata);
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcRHS()
{
	calcRHS(curEigVecs.data(), rhs.data(), tmp1.data(), basisDim);
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
bool SpinDynamic::calcStep()
{
	//TODO adaptive step
	RungeKuttaStep();

	double norm = cblas_dznrm2 (basisDim, curEigVecs.data(), 1);

	MKL_Complex16 invnorm = 1./norm;
	cblas_zscal(basisDim,&invnorm,curEigVecs.data(),1);

	return true;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::RungeKuttaStep()
{
	MKL_Complex16 *eigv=curEigVecs.data();
	MKL_Complex16 *k1dat = k1.data();
	MKL_Complex16 *k2dat = k2.data();
	MKL_Complex16 *k3dat = k3.data();
	MKL_Complex16 *k4dat = k4.data();

	// k1
	calcRHS(eigv, k1dat, tmp1.data(), basisDim);

	// k2
	calcTotalMatrixForTime(time + timeStep/2.);
	sps_zaxpy(basisDim, timeStep/2., k1dat , eigv , tmp1.data() );
	calcRHS(tmp1.data(), k2dat, tmp2.data(), basisDim);

	// k3
	sps_zaxpy(basisDim, timeStep/2., k2dat, eigv, tmp1.data() );
	calcRHS(tmp1.data(), k3dat, tmp2.data(), basisDim);

	// k4
	calcTotalMatrixForTime(time + timeStep);
	sps_zaxpy(basisDim, timeStep, k3dat, eigv , tmp1.data() );
	calcRHS(tmp1.data(), k4dat, tmp2.data(), basisDim);

	MKL_Complex16 stepc=timeStep/6.;
	MKL_Complex16 cc(2.,0.);

	#pragma omp parallel for
	for(int i=0; i<basisDim; i++)
	{
		eigv[i] = eigv[i] + (k1[i] + cc * k2[i] + cc * k3[i] + k4[i]) * stepc ;
	}

}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::eigSolve()
{
	int arnoldiBasisSize = 0;
	if( basisDim < 20 )
	{
		arnoldiBasisSize = 0;
	}

	if( basisDim >= 20 && basisDim < 200 )
	{
		arnoldiBasisSize = 10;
	}

	if( basisDim >= 200 )
	{
		arnoldiBasisSize = 40;
	}

	cout<<"Number of non-zero elements: "<< totmat.nonZeros() <<endl;

    ARCompStdEig<double, sps::SparseMatrixS<MKL_Complex16, Eigen::RowMajor> > dprob_eigen_triangl(totmat.cols(), config.initEigState+1, &totmat,
    																&sps::SparseMatrixS<MKL_Complex16, Eigen::RowMajor>::trheMV,
																	"SR", arnoldiBasisSize, 1E-10, 10000);

    dprob_eigen_triangl.FindEigenvectors();

    int nconv = dprob_eigen_triangl.ConvergedEigenvalues();

    // prints eigenvalues
    Solution(totmat, &sps::SparseMatrixS<MKL_Complex16, Eigen::RowMajor>::trheMV, dprob_eigen_triangl );

    if( config.initEigState < nconv )
    {
    	memcpy(curEigVecs.data(), dprob_eigen_triangl.RawEigenvector(config.initEigState), basisDim * sizeof(MKL_Complex16));

    	energy = dprob_eigen_triangl.Eigenvalue(config.initEigState).real();
    }
    else
    {
    	throw std::runtime_error("Error! Requested eigenpairs are not found");
    }
}






//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcMagMom(int saveInd)
{
	omp_set_nested(0);

	#pragma omp parallel for
	for(int i=0; i< hamBuilder->GetSpinMatrs().size(); i++) // over spins
	{
		MKL_Complex16 val = hamBuilder->GetSpinMatrs()[i].trheVMV(curEigVecs.data(), &tmpvec2[basisDim*i]); //(spinMatrs[i]*vec).dot(vec);

		magMoms[saveInd * hamBuilder->GetSpinMatrs().size() + i] = val.real();

	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcObservables(int saveInd)
{
	if(!calcObserv) return;

	omp_set_nested(0);

	#pragma omp parallel for
	for(int i=0; i < hamBuilder->GetObservMatrs().size(); i++) // over spins
	{
		Eigen::Map<VectorZ> vec(curEigVecs.data(),basisDim);

		MKL_Complex16 val = hamBuilder->GetObservMatrs()[i].trheVMV(curEigVecs.data(), &tmpvec2[basisDim*i]); //(observMatrs[i]*vec).dot(vec);

		observables[saveInd * hamBuilder->GetObservMatrs().size() + i] = val.real();//res.real();
	}
}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcDM(int saveInd)
{
	if( !calcEntropy )
	{
		return;
	}

	omp_set_nested(0);

	int size = densityMatrices.size();

#pragma omp parallel for
	for(int i=0;i<size;i++)
	{
		densityMatrices[i]->CalcDensityMatrix(curEigVecs.data(), basisDim, curDMs[i].data());

/*		cout<<"H"<<endl;
				for(int l = 0; l< densityMatrices[i]->GetDimensions(); l++)
				{
					for(int m=0; m<densityMatrices[i]->GetDimensions(); m++)
					{
						cout<<curDMs[i][l*densityMatrices[i]->GetDimensions()+m]<<" ";
					}
					cout<<endl;
				}
				cout<<endl;
*/
		entropy[saveInd*size + i] = calcVonNeumanEntropy(curDMs[i].data(),curDMeigvals[i].size(),curDMeigvals[i].data());
	}
}

}



