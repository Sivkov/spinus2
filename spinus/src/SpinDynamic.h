/*
 * SpinDynamic.h
 *
 *  Created on: Jul 20, 2015
 *      Author: isivkov
 */

#ifndef SPINDYNAMIC_H_
#define SPINDYNAMIC_H_

#include <utility>
#include <vector>
#include <string>
#include <fstream>
#include <omp.h>

#include "SpinSinTypes.h"
#include <mkl.h>

#include "JJState.h"
#include "Tensor.h"
#include "HamiltonianBuilder.h"
#include "SingleTensor.h"
#include "PairTensor.h"

#include "ZeemanQOp.h"
#include "HeisenbergQOp.h"
#include "IonAnisQOp.h"
#include "SpinQOp.h"

#include "MatrixBuilder.h"
#include "VarParameter.h"
#include "MatrixSparse.h"

#include "SpinDynamicConfig.h"

using namespace std;


namespace sps
{


class SpinDynamic
{
public:
	SpinDynamic();
	virtual ~SpinDynamic();

	void SetJJState(JJState *jjs);
	void SetHamBuilder(HamiltonianBuilder *hb);

	void SetConfig(SpinDynamicConfig &config);

	void Calc();
	//sparse_status_t dd;
protected:
	void paramInit();
	void baseInit();
	void initDM();

	//void outputEigVal();
	void outputMagMom();
	void outputObserv();
	void outputParams();
	void outputDM();

	void calcParamsForStep(int step);
	void calcParamsForTime(double time);

	void calcTotalMatrix();
	void calcTotalMatrixForTime(double time);
	void eigSolve();

	void calcMagMom(int saveInd);
	void saveEnergy(int saveInd);
	void calcObservables(int saveInd);
	void calcDM(int saveInd);

	void calcRHS(MKL_Complex16 *eigvecdata, MKL_Complex16 *rhsdata, MKL_Complex16 *tmpdata, long int size);
	void calcRHS();
	//double calcEnergy(MKL_Complex16 *eigv, MKL_Complex16 *tmp);
	bool calcStep();
	void RungeKuttaStep();

	//-----------------------------------------
	int parLoopIndex{0};
	int maxParDataSize{0};
	long basisDim{0};

	// external
	HamiltonianBuilder *hamBuilder{nullptr};
	JJState *jjState{nullptr};
	//vector<VarParameter*> *params;


	// own config
	SpinDynamicConfig config;
	int numOutSpins{0};
	double damping{0.0};
	double timeStep{0.001};
	int stepsNum{0};
	MKL_Complex16 dampCoef1{0,0};
	MKL_Complex16 dampCoef2{0,0};
	MKL_Complex16 imPlanckCoef{0,0};
	int numSaveSteps{0};
	int saveEverySteps{0};

	// array for all iterations
	vector<double> observables;
	vector<double> magMoms{};
	vector<double> energies;
	vector<double> times;
	vector<double> entropy;

	// rhs of the dynamics diff eq
	vector<MKL_Complex16> rhs;

	// Runge-Kutta vectors
	vector<MKL_Complex16> k1,k2,k3,k4;
	vector<MKL_Complex16> tmp1,tmp2,tmp3,tmp4;

	// variables for current parameter loop interation
	double energy{0.0};
	double time{0.0};
	int saveInd{0};

	// vectors used for current iteration
	vector<double> curMagMoms;
	vector<MKL_Complex16> curParams;
	vector<double> curEigVals;
	vector<MKL_Complex16> curEigVecs;
	vector<vector<MKL_Complex16>> curDMs;
	vector<vector<MKL_Complex16>> curDMeigvals;


	// eigvec of user defined vector (probably)
	vector<MKL_Complex16> startEigVec;

	//vector<SparseMatrixZ> spinMatrs;
	vector<DMAbstract*> densityMatrices;

	// to account temperature distribution
//	vector<double> Zfactors;
//	double Zsum;

	// store parameterized parts of Hamiltonian for quick access
	vector<MKL_Complex16*> parMatDatas;

	// total matrix
	SparseMatrixZ totmat;

	// work arrays setups
	vector<MKL_Complex16> tmpvec2;		// = new MKL_Complex16[dim];

	bool breakCalc{false};
	bool calcObserv{false};
	bool calcEntropy{false};
	bool configured{false};
};

}


#endif /* SPINDYNAMIC_H_ */
