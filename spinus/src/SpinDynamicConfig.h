/*
 * SpinDynamicConfig.h
 *
 *  Created on: Jan 11, 2016
 *      Author: isivkov
 */

#ifndef SPINDYNAMICCONFIG_H_
#define SPINDYNAMICCONFIG_H_

#include <vector>

using namespace std;

namespace sps
{

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
struct SpinDynamicConfig
{
	double timeEnd;
	double timeStep;
	double saveEveryPS;
	double damping;
	vector<int> spinNumsForMagMom;
//	vector<int> spinComponents; // 0-x, 1-y, 2-z
	string magMomOutFile;

	int initEigState;
	bool readInitStateFromFile;
	string initStateFile;

	bool isConfigured;

	string observablesFile;
	string entropyFile;

};


}


#endif /* SPINDYNAMICCONFIG_H_ */
