/*
 * SpinQOp.h
 *
 *  Created on: Jun 11, 2015
 *      Author: isivkov
 */

#ifndef SPINQOP_H_
#define SPINQOP_H_

#include "ZeemanQOp.h"

namespace sps
{

class SpinQOp: public ZeemanQOp
{
public:
	SpinQOp(int numS=0,	int numTot=1) : ZeemanQOp(numS, numTot, -1.0, -1.0, -1.0){}
};

}



#endif /* SPINQOP_H_ */
