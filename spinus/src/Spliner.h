/*
 * Spliner.h
 *
 *  Created on: Jan 14, 2016
 *      Author: isivkov
 */

#ifndef SPLINER_H_
#define SPLINER_H_

#include <vector>
#include "SpinSinTypes.h"

using namespace std;

namespace sps
{

class Spliner
{
public:
	Spliner(int order, double *dataX, double *dataY, int length);

	~Spliner();



private:

	double *dataX;
	double *dataY;
	int nx;
	int ny;

	DFTaskPtr task; // for spline
	MKL_INT xhint; // info about breakpoins
	MKL_INT yhint; // info about function

	// spline parameters
	int order;
	MKL_INT  s_type;     /* Spline type */
	MKL_INT  ic_type;    /* Type of internal conditions */
	double* ic;         /* Array of internal conditions */
	MKL_INT  bc_type;    /* Type of boundary conditions */
	double* bc;         /* Array of boundary conditions */

	vector<double> scoeff;		// [(NX-1)* SPLINE_ORDER];  // Array of spline coefficients
	MKL_INT scoeffhint;    /* Additional information about the coefficients */
};

}



#endif /* SPLINER_H_ */
