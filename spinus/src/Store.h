/*
 * Store.h
 *
 *  Created on: Jan 12, 2016
 *      Author: isivkov
 */

#ifndef STORE_H_
#define STORE_H_

#include <vector>
#include "SpinException.h"
#include <iostream>
#include <string>

using namespace std;

namespace sps
{
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
template<class StoredType>
class AbstractStore
{
public:
	virtual ~AbstractStore(){}

	virtual StoredType* Add(StoredType* obj)=0;

	virtual void Add(vector<StoredType*> &objvec)=0;

	virtual vector<StoredType*>* GetStore()=0;
};

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
template<class StoredType>
class Store : public AbstractStore<StoredType>
{
public:

	Store(string name = ""): name(name) {	}

	virtual ~Store()
	{
		for(auto p: store)
		{
			delete p;
		}
	}

	virtual StoredType* Add(StoredType* obj)
	{
		store.push_back(obj);
		return obj;
	}

	virtual void Add(vector<StoredType*> &objvec)
	{
		store.insert(store.end(),objvec.begin(),objvec.end());
	}

	virtual vector<StoredType*>* GetStore()
	{
		return &store;
	}



private:
	vector<StoredType*> store;
	string name;
};

}



#endif /* STORE_H_ */
