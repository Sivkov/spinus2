/*
 * StoreLocator.h
 *
 *  Created on: Jan 12, 2016
 *      Author: isivkov
 */

#ifndef STORELOCATOR_H_
#define STORELOCATOR_H_

#include "Store.h"

namespace sps
{



template<class StoredType>
class StoreLocator
{
public:
	static AbstractStore<StoredType>* GetStore()
	{
		return _store;
	}

	static void SetStore(AbstractStore<StoredType> *store)
	{
		if(store==nullptr)
		{
			_store=&_nullStore;
		}
		else
		{
			_store = store;
		}
	}

	//static void DisposeStore

private:
	static Store<StoredType> _nullStore;
	static AbstractStore<StoredType> *_store;
};

template<class StoredType>
Store<StoredType> StoreLocator<StoredType>::_nullStore = Store<StoredType>("Default");

template<class StoredType>
AbstractStore<StoredType> *StoreLocator<StoredType>::_store = &StoreLocator<StoredType>::_nullStore;

}




#endif /* STORELOCATOR_H_ */
