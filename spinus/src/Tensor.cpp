/*
 * Tensor.cpp
 *
 *  Created on: Jun 14, 2015
 *      Author: isivkov
 */




#include "Tensor.h"
#include "SpinException.h"

namespace sps
{

bool Tensor::operator==(const Tensor &t2) const
{
	if( initTwoJ != t2.initTwoJ )
	{
		return false;
	}

	if( addTwoJ != t2.addTwoJ )
	{
		return false;
	}

	if( M != t2.M )
	{
		return false;
	}

	return true;
}



bool Tensor::operator!=(const Tensor &t2) const
{
	return !( (*this) == t2 );
}


bool Tensor::operator<(const Tensor &t2) const
{
	if( initTwoJ != t2.initTwoJ )
	{
		return (initTwoJ < t2.initTwoJ);
	}

	if( addTwoJ != t2.addTwoJ )
	{
		return ( addTwoJ < t2.addTwoJ );
	}

	if( M != t2.M )
	{
		return ( M < t2.M );
	}

	return true;
}


bool Tensor::operator>(const Tensor &t2) const
{
	return ( t2 < (*this) );
}



bool Tensor::jjEqual(const Tensor &t2) const
{
	if( initTwoJ != t2.initTwoJ )
	{
		return false;
	}

	if( addTwoJ != t2.addTwoJ )
	{
		return false;
	}

	return true;
}


bool Tensor::jjNotEq(const Tensor &t2) const
{
	return !jjEqual(t2);
}


bool Tensor::jjLess(const Tensor &t2) const
{
	if( initTwoJ != t2.initTwoJ )
	{
		return (initTwoJ < t2.initTwoJ);
	}

	if( addTwoJ != t2.addTwoJ )
	{
		return ( addTwoJ < t2.addTwoJ );
	}

	return true;
}


bool Tensor::jjGreater(const Tensor &t2) const
{
	return t2.jjLess(*this);
}

}
