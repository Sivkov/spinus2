/*
 * Tensor.h
 *
 *  Created on: Jan 16, 2015
 *      Author: isivkov
 */

#ifndef TENSOR_H_
#define TENSOR_H_

//#define __cplusplus 201103L

#include <vector>
#include <string>

#include "SpinSinTypes.h"
//#include <initializer_list>

using namespace std;

namespace sps
{


class Tensor
{
protected:
	vector<int> initTwoJ;	// ranks k1,k2,k3,...,kn of ITO  	{ T(k1) x T(k2) }(k2') x T(k3)}(k3') x ... x T(kn) }(k,M)
	vector<int> addTwoJ;	// ranks k2',k3',...,k of ITO  		{ T(k1) x T(k2) }(k2') x T(k3)}(k3') x ... x T(kn) }(k,M)
	int M;					// component M of ITO				{ T(k1) x T(k2) }(k2') x T(k3)}(k3') x ... x T(kn) }(k,M)
	MKL_Complex16 factor;
	std::string name;

public:
	Tensor(vector<int> mainTwoJ, vector<int> addTwoJ, int M, double factor=1.0, std::string name="None" ):
		initTwoJ(mainTwoJ), addTwoJ(addTwoJ), M(M), factor(factor), name(name)	{	}

	Tensor()
	:Tensor({-1},{-1},1)
	{}
//	Tensor(initializer_list<int> mainTwoJList, initializer_list<int> addTwoJList, int M):
//		mainTwoJ(mainTwoJList), addTwoJ(addTwoJList), M(M) { }

	inline const vector<int> & getInitTwoJ() const { return initTwoJ; }
	inline const vector<int> & getAddTwoJ() const { return addTwoJ; }
	inline int getM() const { return M; }
	inline MKL_Complex16 getFactor() const { return factor; }
	inline string getName() const { return name; }

	// Compare over jj and m
	bool operator==(const Tensor &t2) const;
	bool operator!=(const Tensor &t2) const;
	bool operator<(const Tensor &t2) const;
	bool operator>(const Tensor &t2) const;

	// Compare over jj
	bool jjEqual(const Tensor &t2) const;
	bool jjNotEq(const Tensor &t2) const;
	bool jjLess(const Tensor &t2) const;
	bool jjGreater(const Tensor &t2) const;

};

}
#endif /* TENSOR_H_ */
