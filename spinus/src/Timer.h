/*
 * Timer.h
 * Taken from SIRIUS library
 */

#ifndef SPINUS_SRC_TIMER_H_
#define SPINUS_SRC_TIMER_H_

#include <string>
#include <sstream>
#include <chrono>
#include <map>
#include <vector>
#include <memory>
#include <complex>
#include <algorithm>

struct timer_stats_t
{
    double min_val{1e10};
    double max_val{0};
    double tot_val{0};
    double avg_val{0};
    int count{0};
};

const std::string main_timer_label = "+global_timer";

using time_point_t = std::chrono::high_resolution_clock::time_point;

class Timer
{
private:

	std::string label_;
	time_point_t starting_time_;
	bool active_{false};

	static std::vector<std::string>& stack()
    {
		static std::vector<std::string> stack_;
		return stack_;
    }

	static std::map<std::string, std::map<std::string, double>>& timer_values_ex()
	{
		static std::map<std::string, std::map<std::string, double>> timer_values_ex_;
		return timer_values_ex_;
    }

public:

	Timer(std::string label__)
	: label_(label__)
	{
			starting_time_ = std::chrono::high_resolution_clock::now();
			stack().push_back(label_);
			active_ = true;
	}

	~Timer()
	{
		stop();
	}

	static std::map<std::string, timer_stats_t>& timer_values()
	{
		static std::map<std::string, timer_stats_t> timer_values_;
		return timer_values_;
	}

	double stop()
	{
		if (!active_) {
			return 0;
		}

		/* remove this timer name from the list */
		stack().pop_back();

		auto t2    = std::chrono::high_resolution_clock::now();
		auto tdiff = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - starting_time_);
		double val = tdiff.count();

		auto& ts = timer_values()[label_];
		ts.min_val = std::min(ts.min_val, val);
		ts.max_val = std::max(ts.max_val, val);
		ts.tot_val += val;
		ts.count++;

		if (stack().size() != 0) {
			/* now last element contains the name of the parent timer */
			auto parent_label = stack().back();
			if (timer_values_ex().count(parent_label) == 0) {
				timer_values_ex()[parent_label] = std::map<std::string, double>();
			}
			if (timer_values_ex()[parent_label].count(label_) == 0) {
				timer_values_ex()[parent_label][label_] = 0;
			}
			timer_values_ex()[parent_label][label_] += val;
		}
		active_ = false;
		return val;
	}

	static void print()
	{
		for (int i = 0; i < 140; i++) {
			printf("-");
		}
		printf("\n");
		printf("name                                                                 count      total        min        max    average    self (%%)\n");
		for (int i = 0; i < 140; i++) {
			printf("-");
		}
		printf("\n");
		for (auto& it: timer_values()) {

			double te{0};
			if (timer_values_ex().count(it.first)) {
				for (auto& it2: timer_values_ex()[it.first]) {
					te += it2.second;
				}
			}
			if (it.second.tot_val > 0.01) {
				printf("%-65s : %6i %10.4f %10.4f %10.4f %10.4f     %6.2f\n", it.first.c_str(),
						it.second.count,
						it.second.tot_val,
						it.second.min_val,
						it.second.max_val,
						it.second.tot_val / it.second.count,
						(it.second.tot_val - te) / it.second.tot_val * 100);
			}
		}
	}

	static void print_tree()
	{
		if (!timer_values().count(main_timer_label)) {
			return;
		}
		for (int i = 0; i < 140; i++) {
			printf("-");
		}
		printf("\n");

		double ttot = timer_values()[main_timer_label].tot_val;

		for (auto& it: timer_values()) {
			if (timer_values_ex().count(it.first)) {
				/* collect external times */
				double te{0};
				for (auto& it2: timer_values_ex()[it.first]) {
					te += it2.second;
				}
				double f = it.second.tot_val / ttot;
				if (f > 0.01) {
					printf("%s (%10.4fs, %.2f %% of self, %.2f %% of total)\n",
							it.first.c_str(), it.second.tot_val, (it.second.tot_val - te) / it.second.tot_val * 100, f * 100);

					std::vector<std::pair<double, std::string>> tmp;

					for (auto& it2: timer_values_ex()[it.first]) {
						tmp.push_back(std::pair<double, std::string>(it2.second / it.second.tot_val, it2.first));
					}
					std::sort(tmp.rbegin(), tmp.rend());
					for (auto& e: tmp) {
						printf("|--%s (%10.4fs, %.2f %%) \n", e.second.c_str(), timer_values_ex()[it.first][e.second], e.first * 100);
					}
				}
			}
		}
	}
};



#endif /* SPINUS_SRC_TIMER_H_ */
