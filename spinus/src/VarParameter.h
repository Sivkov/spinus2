/*
 * VarParameter.h
 *
 *  Created on: Jun 3, 2015
 *      Author: isivkov
 */

#ifndef VARPARAMETER_H_
#define VARPARAMETER_H_

#include <vector>
#include <string>
#include "SpinException.h"
#include "SpinSinTypes.h"
#include "QOperator.h"

namespace sps
{



class VarParameter
{
public:
	enum State
	{
		Static,
		Dynamic,
		Fixed
	};

	VarParameter(std::string name, State state);

	~VarParameter();

	// Tensor operations ---------------------------------------
	void AddTensor(Tensor *tensor);

	void AddTensor(const std::vector<Tensor*> &tensors);

	void AddTensorsFromQOperator(QOperator* qOp);

	void AddTensorsFromQOperator(const std::vector<QOperator*> &qOps);

	void RemoveTensors(const std::vector<Tensor*> &tensors);

	const std::vector<Tensor*> GetTensors() const;

	// Parameter Data for spin dynamics or multi static calc ----
	inline const vector<double>& GetParData(){		return parData; 	}
	inline double GetArgStep(){ return argStep; }

	void SetArgStep(double step){ argStep=step; }
	void SetParData(vector<double>&& parData){ this->parData = std::move(parData); }

	inline const SparseMatrixZ& GetMatrix() const {		return pTensorMatrix;	}
	inline       SparseMatrixZ& GetMatrixUnsafe() {		return pTensorMatrix;	}

	string GetName(){ return name; }

	State GetState(){	return state;	}

	void SetState(State state){	this->state=state;	}

private:
	vector<double> parData;
//	vector<double> argData;
	double argStep;
	std::vector<Tensor*> varTensors;
	std::string name;
	State state;
	SparseMatrixZ pTensorMatrix;

};

}


#endif /* VARPARAMETER_H_ */
