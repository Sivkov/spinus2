/*
 * Vector.h
 *
 *  Created on: Dec 12, 2014
 *      Author: isivkov
 */


#ifndef VECTOR_UNSAFE_H_
#define VECTOR_UNSAFE_H_

namespace sps
{

namespace unsafe
{



template<class type>
class Vector
{
public:

	Vector(){data=nullptr; size=0; dontFree=false;}
	Vector(long int length);
	Vector(type *data, long int vectorSize, bool dontFree=false);
	virtual ~Vector();

	void SetData(type *data, long int vectorSize);
	type* GetData();
	const type* GetData() const;
	long int GetSize() const;

	void DontFreeMemory(bool dont);

	virtual void Clear();

	void ZeroMemory();

protected:
	type *data;
	long int size;
	bool dontFree;
};





//-------------------------------------------------------------------------
	template<class type>
	Vector<type>::Vector(long int length)
	{
		this->size=length;
		data = new type [length];
		dontFree=false;
	}

	//-------------------------------------------------------------------------
	template<class type>
	Vector<type>::Vector(type *data, long int vectorSize,bool dontFree)
	{
		this->data=data;
		this->size=vectorSize;
		this->dontFree=dontFree;
	}

	//-------------------------------------------------------------------------
	template<class type>
	void Vector<type>::Clear()
	{
		if(data!=nullptr)
		{
			delete [] data;
			data=nullptr;
		}

		this->size=0;
	}

	//-------------------------------------------------------------------------
	template<class type>
	Vector<type>::~Vector()
	{
		if(data!=nullptr && dontFree==true)
			delete [] data;
	}


	//-------------------------------------------------------------------------
	template<class type>
	void Vector<type>::SetData(type *data, long int vectorSize)
	{
		this->data=data;
		this->size=vectorSize;
		dontFree=true;
	}


	//-------------------------------------------------------------------------
	template<class type>
	void Vector<type>::DontFreeMemory(bool dont)
	{
		dontFree=dont;
	}


	//-------------------------------------------------------------------------
	template<class type>
	type* Vector<type>::GetData()
	{
		return data;
	}

	//-------------------------------------------------------------------------
	template<class type>
	const type* Vector<type>::GetData() const
	{
		return data;
	}

	//-------------------------------------------------------------------------
	template<class type>
	long int Vector<type>::GetSize() const
	{
		return size;
	}

	//-------------------------------------------------------------------------
	template<class type>
	void Vector<type>::ZeroMemory()
	{
		memset(data,0,sizeof(type)*size);
	}


}


}



#endif /* VECTOR_H_ */
