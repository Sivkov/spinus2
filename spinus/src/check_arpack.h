/*
 * check_arpack.h
 *
 *  Created on: Nov 18, 2017
 *      Author: isivkov
 */

#ifndef SPINUS_SRC_CHECK_ARPACK_H_
#define SPINUS_SRC_CHECK_ARPACK_H_

#include "arcomp.h"
#include <cmath>
#include "blas1c.h"
#include "lapackc.h"
//#include "matprod.h"
#include "arscomp.h"

template<class ARMATRIX, class ARFLOAT, class ARFOP>
void Solution(ARMATRIX &A, void (ARFOP::* MultOPxp)(arcomplex<ARFLOAT>[],arcomplex<ARFLOAT>[]), ARCompStdEig<ARFLOAT, ARMATRIX> &Prob)
/*
 Prints eigenvalues and eigenvectors of complex eigen-problems
 on standard "std::cout" stream.
 */

{

	int                i, n, nconv, mode;
	arcomplex<ARFLOAT> *Ax;
	ARFLOAT            *ResNorm;

	n     = Prob.GetN();
	nconv = Prob.ConvergedEigenvalues();
	mode  = Prob.GetMode();

	std::cout << std::endl << std::endl << "Checking ARPACK++ \n";
	std::cout << "Complex eigenvalue problem: A*x - lambda*x" << std::endl;
	switch (mode) {
  case 1:
			std::cout << "Regular mode" << std::endl << std::endl;
			break;
  case 3:
			std::cout << "Shift and invert mode" << std::endl << std::endl;
	}

	std::cout << "Dimension of the system            : " << n              << std::endl;
	std::cout << "Number of 'requested' eigenvalues  : " << Prob.GetNev()  << std::endl;
	std::cout << "Number of 'converged' eigenvalues  : " << nconv          << std::endl;
	std::cout << "Number of Arnoldi vectors generated: " << Prob.GetNcv()  << std::endl;
	std::cout << "Number of iterations taken         : " << Prob.GetIter() << std::endl;
	std::cout << std::endl;

	if (Prob.EigenvaluesFound()) {

		// Printing eigenvalues.

		std::cout << "Eigenvalues:" << std::endl;
		for (i=0; i<nconv; i++) {
			std::cout << "  lambda[" << (i+1) << "]: " << Prob.Eigenvalue(i) << std::endl;
		}
		std::cout << std::endl;
	}

	if (Prob.EigenvectorsFound()) {

		// Printing the residual norm || A*x - lambda*x ||
		// for the nconv accurately computed eigenvectors.

		Ax      = new arcomplex<ARFLOAT>[n];
		ResNorm = new ARFLOAT[nconv+1];

		for (i=0; i<nconv; i++) {
			(A.*MultOPxp)(Prob.RawEigenvector(i),Ax);
			axpy(n, -Prob.Eigenvalue(i), Prob.RawEigenvector(i), 1, Ax, 1);
			ResNorm[i] = nrm2(n, Ax, 1)/
			lapy2(real(Prob.Eigenvalue(i)),imag(Prob.Eigenvalue(i)));
		}

		for (i=0; i<nconv; i++) {
			std::cout << "||A*x(" << (i+1) << ") - lambda(" << (i+1);
			std::cout << ")*x(" << (i+1) << ")||: " << ResNorm[i] << std::endl;
		}
		std::cout << "\n";

		delete[] Ax;
		delete[] ResNorm;

	}

} // Solution

#endif /* SPINUS_SRC_CHECK_ARPACK_H_ */
