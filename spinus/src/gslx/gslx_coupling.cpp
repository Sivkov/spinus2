/*
 * gslx_coupling.cpp
 *
 *  Created on: Jan 26, 2015
 *      Author: isivkov
 */
#include "gslx_coupling.h"

namespace gslx
{


double coupling_9j(int twoJconf[9])
{
	double res=0.0;

	res=gsl_sf_coupling_9j(twoJconf[0],twoJconf[1],twoJconf[2],
						   twoJconf[3],twoJconf[4],twoJconf[5],
						   twoJconf[6],twoJconf[7],twoJconf[8]);
	return res;
}

double coupling_3j(int iwoJ1, int twoJ2, int twoJ3, int twoM1, int twoM2, int twoM3)
{
	return 0.0;
}

}
