//============================================================================
// Name        : test.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <unistd.h>
#include <iomanip>
#include <time.h>
#include <cstring>
//#include <cilk/cilk.h>
//#include <cilk/cilk_api.h>
#include <HamiltonianBuilder.h>
#include <math.h>
#include <fstream>

#include <functional>
//-------------------------

//#include <mkl.h>

#include <Game.h>

using namespace std;
using namespace sps;


int main(int argc, char *argv[]) {

	Game *game=new Game();

//	try
//	{
		if(argc<2) throw SpinException("error, specify input file");

		game->Run(argv[1]);
//	}
//	catch(exception &ex)
//	{
//		cout<<ex.what()<<endl;
//		std::exit(1);
//	}

	delete game;

	return 0;
}
