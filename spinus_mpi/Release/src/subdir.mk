################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/spinus_mpi.cpp 

OBJS += \
./src/spinus_mpi.o 

CPP_DEPS += \
./src/spinus_mpi.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpiicpc -std=c++0x -I"/scratch/isivkov/DevProj2/spinsin/spinus/src" -I/cluster/intel_2013SP1/composer_xe_2013_sp1.0.080/compiler/include -I/cluster/intel_2013SP1/composer_xe_2013_sp1.0.080/mkl/include -I/opt/intel/composer_xe_2013_sp1.0.080/compiler/include -I/opt/intel/composer_xe_2013_sp1.0.080/mkl/include -O3 -Wall -c -fmessage-length=0 -mt_mpi -openmp  -vec-report2 -xHost -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


